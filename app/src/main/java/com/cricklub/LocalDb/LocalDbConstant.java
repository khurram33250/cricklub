package com.cricklub.LocalDb;

public class LocalDbConstant {
    public static String DB_NAME = "cricklub.db";
    public static int DB_VERSION = 15;
    //player
    public static String PLAYER_TABLE = "player";
    public static String PLAYER_ID = "id";
    public static String PLAYER_NAME = "name";
    public static String PLAYER_EMAIL = "player_email";
    public static String PHONE_NO = "phone_no";
    public static String IS_APP_USER = "is_app_user";
    public static String PLAYER_CATEGORY = "player_category";


    public static String PLAYER_TABLE_QUERY = "CREATE TABLE " + LocalDbConstant.PLAYER_TABLE + " (" + LocalDbConstant.PLAYER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            LocalDbConstant.PLAYER_NAME + " TEXT, " + LocalDbConstant.PLAYER_EMAIL + " TEXT ," + LocalDbConstant.PHONE_NO + " TEXT ," + LocalDbConstant.IS_APP_USER + " TEXT ," + LocalDbConstant.PLAYER_CATEGORY + " TEXT )";

    //match table
    public static String START_MATCH = "match_table";
    public static String MATCH_ID = "match_id";
    public static String MATCH_NAME = "match_name";
    public static String MATCH_STAGE = "match_stage";
    public static String BATTING_TEAM = "batting_team";
    public static String BOWLING_TEAM = "bowling_team";
    public static String VENUE = "venue";
    public static String TOSS_WON_BY = "toss_won_by";
    public static String TOTAL_OVERS = "overs";
    public static String WINNNER = "winner";
    public static String CURRENT_INNINGS = "current_status";
    public static String IS_COMPLETED = "is_completed";
    public static String OPPORENT_SCORE = "oppoent_score";//1 means current
    public static String NO_OF_PLAYERS = "no_of_players";
    public static String CURRENT_DATE = "current_date";
    public static String MODIFY_DATE = "modify_date";
    public static String START_MATCH_QUERY = "CREATE TABLE " + LocalDbConstant.START_MATCH + " (" + LocalDbConstant.MATCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            LocalDbConstant.MATCH_NAME + " TEXT, " + LocalDbConstant.CURRENT_INNINGS + " TEXT, " + LocalDbConstant.MATCH_STAGE + " TEXT, "
            + LocalDbConstant.VENUE + " TEXT," + LocalDbConstant.TOSS_WON_BY + " TEXT," + LocalDbConstant.BATTING_TEAM + " TEXT ,"
            + LocalDbConstant.BOWLING_TEAM + " TEXT ," + LocalDbConstant.TOTAL_OVERS + " TEXT ," +
            LocalDbConstant.WINNNER + " TEXT ," + LocalDbConstant.OPPORENT_SCORE + " TEXT ," + LocalDbConstant.IS_COMPLETED + " TEXT," + LocalDbConstant.NO_OF_PLAYERS + " TEXT ," + LocalDbConstant.CURRENT_DATE + " TEXT ," + LocalDbConstant.MODIFY_DATE + " TEXT )";

    //current_match_player
    public static String CURR_MATCH_PLAYER_TABLE = "curr_match_table_table";
    public static String MATCH_PLAYER_ID = "match_player_id";
    public static String MATCH_PLAYER_INNINGS = "match_player_innings";
    public static String MATCH_PLAYER_STATUS = "match_player_status";  //1 player 2 out 3 bowling
    public static String MATCH_PLAYER_BAT_ID = "matchplayerid";
    public static String MATCH_PLAYER_SCORE = "match_player_score";
    public static String PLAYER_TABLE_MATCH_ID = "match_if_player_table";
    public static String CURRE_MATCH_PLAYER_MAT_ID="current_match_id";

    public static String CURR_MATCH_PLAYER_TABLE_QUERY = "CREATE TABLE " + LocalDbConstant.CURR_MATCH_PLAYER_TABLE + " (" + LocalDbConstant.MATCH_PLAYER_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT," +
            LocalDbConstant.MATCH_PLAYER_INNINGS + " TEXT, " + LocalDbConstant.PLAYER_TABLE_MATCH_ID + " TEXT, " + LocalDbConstant.MATCH_PLAYER_STATUS + " TEXT, "
            + LocalDbConstant.MATCH_PLAYER_BAT_ID + " TEXT, " + LocalDbConstant.MATCH_PLAYER_SCORE + " TEXT,"+LocalDbConstant.CURRE_MATCH_PLAYER_MAT_ID+" TEXT )";


    //match_over_table

    public static String MATCH_OVER_TABLE = "match_over_table";
    public static String MATCH_OVER_ID = "match_over_id";
    public static String MATCH_OVER_MATCH_ID = "match_over_match_id";
    public static String MATCH_OVER_CREATE_AT = "match_over_created_at";
    public static String MATCH_OVER_INNINGS = "match_over_innings";
    public static String MATCH_OVER_MODIFY = "match_over_modify_at";

    public static String MATCH_OVER_TABLE_QUERY = "CREATE TABLE " + LocalDbConstant.MATCH_OVER_TABLE + " (" + LocalDbConstant.MATCH_OVER_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT," +
            LocalDbConstant.MATCH_OVER_MATCH_ID + " TEXT, " + LocalDbConstant.MATCH_OVER_CREATE_AT + " TEXT, "
            + LocalDbConstant.MATCH_OVER_INNINGS + " TEXT, " + LocalDbConstant.MATCH_OVER_MODIFY + " TEXT )";


    //score_board
    public static String MATCH_SCORE_BOARD = "match_score_board";
    public static String MATCH_SCORE_BOARD_OVER_ID = "score_board_over_id";
    public static String SCORE_BOARD_PLAYER_ID = "score_board_player_id";
    public static String SCORE_BOARD_BALLS = "score_board_balls"; //0 means ball counted 1 means not counted
    public static String SCORE_BOARD_SCORE = "score_board_score";
    public static String SCORE_BOARD_BALL_CAT = "score_board_ball_cat";

    public static String MATCH_SCORE_BOARD_QUERY = "CREATE TABLE " + LocalDbConstant.MATCH_SCORE_BOARD + " (" + LocalDbConstant.MATCH_SCORE_BOARD_OVER_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT," +
            LocalDbConstant.SCORE_BOARD_PLAYER_ID + " TEXT, " + LocalDbConstant.SCORE_BOARD_BALLS + " TEXT, "
            + LocalDbConstant.SCORE_BOARD_SCORE + " TEXT, " + LocalDbConstant.SCORE_BOARD_BALL_CAT + " TEXT )";


    //PLAYER SELECTED
    public static String SELECTED_PLAYER_TABLE = "selected_player_table";
    public static String SELECTED_PLAYER_ID = "selected_player_id";
    public static String MATCH_ID_FOR_MATCH = "match_id_for_match";
    public static String PLAYER_ID_FOR_MATCH = "player_id_for_match";
    public static String SELECTED_PLAYER_STATE = "selected_player_state";
    public static String SELECTED_CREATED_AT = "selected_player_created_at";

    public static String SELECTED_PLAYER_TABLE_QUERY = "CREATE TABLE " + LocalDbConstant.SELECTED_PLAYER_TABLE + " (" + LocalDbConstant.SELECTED_PLAYER_ID +
            " INTEGER PRIMARY KEY AUTOINCREMENT," +
            LocalDbConstant.MATCH_ID_FOR_MATCH + " TEXT, " + LocalDbConstant.PLAYER_ID_FOR_MATCH + " TEXT, "
            + LocalDbConstant.SELECTED_PLAYER_STATE + " TEXT, " + LocalDbConstant.SELECTED_CREATED_AT + " TEXT )";


}
