package com.cricklub.LocalDb;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    public DbHelper(@Nullable Context context) {
        super(context, LocalDbConstant.DB_NAME, null, LocalDbConstant.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LocalDbConstant.PLAYER_TABLE_QUERY);
        db.execSQL(LocalDbConstant.START_MATCH_QUERY);
        db.execSQL(LocalDbConstant.CURR_MATCH_PLAYER_TABLE_QUERY);
        db.execSQL(LocalDbConstant.MATCH_OVER_TABLE_QUERY);
        db.execSQL(LocalDbConstant.MATCH_SCORE_BOARD_QUERY);
        db.execSQL(LocalDbConstant.SELECTED_PLAYER_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + LocalDbConstant.PLAYER_TABLE);
        db.execSQL(LocalDbConstant.PLAYER_TABLE_QUERY);
        db.execSQL("DROP TABLE IF EXISTS " + LocalDbConstant.START_MATCH);
        db.execSQL(LocalDbConstant.START_MATCH_QUERY);
        db.execSQL("DROP TABLE IF EXISTS " + LocalDbConstant.CURR_MATCH_PLAYER_TABLE);
        db.execSQL(LocalDbConstant.CURR_MATCH_PLAYER_TABLE_QUERY);

        db.execSQL("DROP TABLE IF EXISTS " + LocalDbConstant.MATCH_OVER_TABLE);
        db.execSQL(LocalDbConstant.MATCH_OVER_TABLE_QUERY);

        db.execSQL("DROP TABLE IF EXISTS " + LocalDbConstant.MATCH_SCORE_BOARD);
        db.execSQL(LocalDbConstant.MATCH_SCORE_BOARD_QUERY);

        db.execSQL("DROP TABLE IF EXISTS " + LocalDbConstant.SELECTED_PLAYER_TABLE);
        db.execSQL(LocalDbConstant.SELECTED_PLAYER_TABLE_QUERY);


    }
}
