package com.cricklub;

public class CONSTANT {
    //Application flow
    public static String APP_STATE = "app_state";

    // 1 means login
    //2 means go to Home Activity
    //3 means create team Activity


    public static String BASE_URL = "http://cricklub.com/";
    public static String PREF_NAME = "cricklub";
    public static String SIGN_UP = "signup-api";
    public static String LOGIN_IN = "login-api";
    public static String UNIQUE_ID = "unique_id";
    public static String NAME = "name";
    public static String PHONE_NUMBER = "phone_number";
    public static String EMAIL = "email";
    public static String IS_CAPTAIN = "is_captain";
    public static String CONFIRMATION_CODE = "confirmation_code";
    public static String PLAYER_ID = "player_id";
    public static String VERIFICATION = "code-checking-api";
    public static String FORGOT_API_NO = "forgotpassword-api";
    public static String UPDATE_FORGOT_API = "update-password-api";
    public static String CHANGE_PASSWORD = "change-password-api";
    public static String UPDATE_TEAM_NAME = "update-team-settings";
    public static String CREATE_TEAM_NAME = "api-add-team";

    public static String SEND_REQUEST="team-list-api-for-cricklub";

    public static String UPDATE_USER_PROFILE = "update-the-user-api";
    public static String API_STATUS = "status";
    public static String API_MESSAGE = "message";
    public static String RESEND_CODE = "resend-confirmation-code";

    public static String IMAGES_BASE_URL = "http://cricklub.com/uploads/player/";


    //Local constants
    public static String BATTING_TEAM = "batting_team";
    public static String BOWLING_TEAM = "bowling_team";

    public static String DECISION_VALUE_BATTING = "Batting";
    public static String DECISION_VALUE_BOWLING = "Bowling";
    public static String CURRENT_STATUS = "current_status"; //1 means

    public static String CURRENT_FRAGMENT = "current_fragment";   //1 means scoreBoard


    //tream details
    public static String TEAM_ID = "team_id";
    public static String TEAM_NAME = "team_name";
    public static String TEAM_CITY = "team_city";
    public static String TEAM_LOGO = "team_logo";
    public static String TEAM_PROFILE = "team_profile";


}
