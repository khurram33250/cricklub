package com.cricklub.fragments;


import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.cricklub.AppUtils;
import com.cricklub.CONSTANT;
import com.cricklub.R;
import com.cricklub.RealPathUtil;
import com.cricklub.WebReq.WebReqClass;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;

/**
 * A simple {@link Fragment} subclass.
 */
public class UpdateProfileFragment extends Fragment {
    TextInputLayout address, name, shirtNo, email;
    TextInputEditText addressEdittext, nameEdittext, shirtNoEdittext, emailEdittext;
    Button updateButton;
    ImageView profileImageView;
    RadioGroup playerTypeRg;
    int playerType = 1;
    int PERSON = 2;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus, per;
    String TAG = "CreateTeamActivity";
    ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    String profileImage;
    RadioButton captain, player;


    public UpdateProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        setHasOptionsMenu(true);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);


        getActivity().setTitle("Update Profile");
        permissionStatus = getActivity().getSharedPreferences("permissionStatus", Context.MODE_PRIVATE);
        sharedPreferences = getActivity().getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        final View rootView = inflater.inflate(R.layout.fragment_update_profile, container, false);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);
        address = rootView.findViewById(R.id.address);
        email = rootView.findViewById(R.id.email);
        name = rootView.findViewById(R.id.nameET);
        shirtNo = rootView.findViewById(R.id.shirtNo);
        addressEdittext = rootView.findViewById(R.id.addressEt);
        emailEdittext = rootView.findViewById(R.id.emailEt);
        shirtNoEdittext = rootView.findViewById(R.id.shirtNoEt);
        updateButton = rootView.findViewById(R.id.continueBtn);
        nameEdittext = rootView.findViewById(R.id.nameEdittext);
        playerTypeRg = rootView.findViewById(R.id.playerTypeRg);
        profileImageView = rootView.findViewById(R.id.profileIv);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        playerTypeRg = rootView.findViewById(R.id.playerTypeRg);
        sharedPreferences = getActivity().getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        captain = rootView.findViewById(R.id.captain);
        player = rootView.findViewById(R.id.player);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nameEdittext.getText().toString().length() > 0 && addressEdittext.getText().toString().length() > 0 && shirtNoEdittext.getText().toString().length() > 0) {
                    if (AppUtils.isOnline(getActivity())) {
                        RequestParams requestParams = new RequestParams();
                        requestParams.put("name", nameEdittext.getText().toString());
                        requestParams.put("email", emailEdittext.getText().toString());
                        requestParams.put("address", addressEdittext.getText().toString());
                        requestParams.put("shirtNo", shirtNoEdittext.getText().toString());
                        requestParams.put("playerID", sharedPreferences.getString(CONSTANT.PLAYER_ID, ""));
                        try {
                            if (profileImage != null)
                                requestParams.put("image", new File(profileImage));
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        WebReqClass.post(CONSTANT.UPDATE_USER_PROFILE, requestParams, new UpdateProfile());
                    } else {
                        Toast.makeText(getActivity(), "No Internet Connection Found!", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        if (sharedPreferences.getBoolean(CONSTANT.IS_CAPTAIN, false)) {
            captain.setChecked(true);
        } else {
            player.setChecked(true);
        }


        profileImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String requiredPermission = "android.permission.READ_EXTERNAL_STORAGE";
                int checkVal = getActivity().checkCallingOrSelfPermission(requiredPermission);
                if (checkVal == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, PERSON);
                } else {
                    permissionDialog();
                }
            }
        });
        playerTypeRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = rootView.findViewById(checkedId);
                if (radioButton.getText().equals("Captain")) {
                    playerType = 1;
                } else {
                    playerType = 2;
                }
            }
        });

        addressEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (addressEdittext.getText().toString().length() > 0) {
                    address.setError("");
                }
                if (nameEdittext.getText().toString().length() > 0) {
                    name.setError("");
                }
                if (emailEdittext.getText().toString().length() > 0) {
                    email.setError("");
                }
                if (shirtNoEdittext.getText().toString().length() > 0) {
                    shirtNo.setError("");
                }


            }
        });

        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getActivity(), "Unable to get Permission", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PERSON && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            profileImageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

            profileImage = RealPathUtil.getRealPath(getActivity(), data.getData());

        }
    }

    private void permissionDialog() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getActivity(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, true);
            editor.commit();


        }

    }

    public class UpdateProfile extends JsonHttpResponseHandler {
        @Override
        public void onStart() {
            super.onStart();
            progressDialog.show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            Log.e(TAG, response.toString());
            if (statusCode == 200) {
                try {
                    String status = response.getString(CONSTANT.API_STATUS);
                    String message = response.getString(CONSTANT.API_MESSAGE);
                    if (status.equalsIgnoreCase("201")) {
                        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(getActivity(), "" + getResources().getString(R.string.messageIssue), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
            Log.e(TAG,throwable.getLocalizedMessage());
            progressDialog.dismiss();
        }

        @Override
        public void onFinish() {
            super.onFinish();
            progressDialog.dismiss();
        }
    }
}
