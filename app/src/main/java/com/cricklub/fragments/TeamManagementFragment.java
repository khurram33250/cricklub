package com.cricklub.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.LocalDb.DbHelper;
import com.cricklub.LocalDb.LocalDbConstant;
import com.cricklub.R;
import com.cricklub.activities.AddPlayerManual;
import com.cricklub.adapters.TeamManagementAdapter;
import com.cricklub.models.SelectedPlayer;
import com.cricklub.models.SendRequest;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class TeamManagementFragment extends Fragment implements TeamManagementAdapter.RemovedPlayerListener {

    RecyclerView teamManagementRecyclerView;
    TeamManagementAdapter teamManagementAdapter;
    FloatingActionButton addPlayer;
    private DbHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;
    private List<SelectedPlayer> playersList;
    private String playerTableName;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("Team Management");
        View view = inflater.inflate(R.layout.fragment_team_management, container, false);
        getActivity().findViewById(R.id.fab).setVisibility(View.GONE);

        playersList = new ArrayList<>();
        playerTableName = LocalDbConstant.PLAYER_TABLE;

        teamManagementRecyclerView = view.findViewById(R.id.teamManagemetRecyclerView);
        teamManagementAdapter = new TeamManagementAdapter(getActivity(), playersList);
        teamManagementRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        teamManagementRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        teamManagementRecyclerView.setAdapter(teamManagementAdapter);
        addPlayer = view.findViewById(R.id.addPlayer);
        addPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddPlayerManual.class));
            }
        });

        getPlayers();

        return view;
    }

    private void getPlayers() {

        dbHelper = new DbHelper(getActivity());
        sqLiteDatabase = dbHelper.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + playerTableName, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    String id = cursor.getString(cursor.getColumnIndex(LocalDbConstant.PLAYER_ID));
                    String name = cursor.getString(cursor.getColumnIndex(LocalDbConstant.PLAYER_NAME));
                    String isAppUser = cursor.getString(cursor.getColumnIndex(LocalDbConstant.IS_APP_USER));
                    String phoneNumber = cursor.getString(cursor.getColumnIndex(LocalDbConstant.PHONE_NO));

                    playersList.add(new SelectedPlayer(id, name, false, phoneNumber, isAppUser));
                    cursor.moveToNext();
                }
            }
            teamManagementAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRemovePlayerClick() {

    }

    public class RemovePlayerApi extends JsonHttpResponseHandler {
        @Override
        public void onStart() {
            super.onStart();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
            super.onSuccess(statusCode, headers, response);
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject jsonObject = response.getJSONObject(i);
                    SendRequest sendRequest = new SendRequest(jsonObject.getString("teamID"), jsonObject.getString("teamUnique"), jsonObject.getString("name"), jsonObject.getString("city"), jsonObject.getString("logo"), jsonObject.getString("profileImage"), jsonObject.getString("created_at"));
                    sendRequestList.add(sendRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sendRequestToCaptainAdapter.notifyDataSetChanged();

        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
        }

        @Override
        public void onFinish() {
            super.onFinish();
        }
    }
}
