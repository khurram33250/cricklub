package com.cricklub.fragments;


import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.cricklub.LocalDb.DbHelper;
import com.cricklub.LocalDb.LocalDbConstant;
import com.cricklub.LocalDb.QueriesTable;
import com.cricklub.models.NameandIdModel;
import com.cricklub.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ScoreBoardMainFragment extends Fragment implements View.OnClickListener {
    Button dottedMenu;

    TextView currentRunRate, requireRunRate, extraScore, opponentTeamScore, yourTeamScore, thisOver;
    LinearLayout playerOne, playerTwo;
    Button number7, number5, number6, number4, number1, numberW, number3, number2, number0, buttonUndo, numberLb, numberBold, numberNoBall, numberWhite;

    //score for players
    TextView playerOneRun;
    RadioButton playeroneRadio, playertwoRadio;
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;

    long currentMatchId;
    String TAG = "scoreBoardMainFragment";
    String currentPlaying;

    public ScoreBoardMainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        List<String> playersId = new ArrayList<>();
       /* Cursor getCurrentPlayingPlayer = sqLiteDatabase.rawQuery(QueriesTable.getPlayerId(currentMatchId, "1"), null);
        Log.e(TAG, QueriesTable.getPlayerId(currentMatchId, "1") + "first query");*/
       /* if (getCurrentPlayingPlayer != null && getCurrentPlayingPlayer.getCount() > 0) {
            if (getCurrentPlayingPlayer.moveToFirst()) {
                do {
                    playersId.add(getCurrentPlayingPlayer.getString(getCurrentPlayingPlayer.getColumnIndex(LocalDbConstant.PLAYERID)));
                } while (getCurrentPlayingPlayer.moveToNext());
            }
        }*/
        Log.e(TAG, playersId.size() + " playerId size");
       /* if (!getCurrentPlayingPlayer.isClosed()) {
            getCurrentPlayingPlayer.close();
        }*/
        List<NameandIdModel> playersName = new ArrayList<>();
        for (String id : playersId) {
            Cursor cursor = sqLiteDatabase.rawQuery(QueriesTable.getPlayerRecord(id), null);
            if (cursor != null && cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        NameandIdModel nameandIdModel = new NameandIdModel();
                        nameandIdModel.setId(id);
                        nameandIdModel.setName(cursor.getString(cursor.getColumnIndex(LocalDbConstant.PLAYER_NAME)));
                        playersName.add(nameandIdModel);
                    } while (cursor.moveToNext());
                }
            }
            Log.e(TAG, playersName.size() + " playerName size");

            if (!cursor.isClosed()) {
                cursor.close();
            }
        }
        if (playersName.size() > 0) {
            currentPlaying = playersName.get(0).getId();
            playerOne.setTag(playersName.get(0));
            playerTwo.setTag(playersName.get(1));
            playeroneRadio.setText(playersName.get(0).getName());
            playertwoRadio.setText(playersName.get(1).getName());
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_score_board_main, container, false);
        dottedMenu = view.findViewById(R.id.dottedMenu);
        currentRunRate = view.findViewById(R.id.currentRunRate);
        requireRunRate = view.findViewById(R.id.requireRunRate);
        extraScore = view.findViewById(R.id.extraScore);
        opponentTeamScore = view.findViewById(R.id.opponentTeamScore);
        yourTeamScore = view.findViewById(R.id.yourTeamScore);
        playerOne = view.findViewById(R.id.playerOne);
        playeroneRadio = view.findViewById(R.id.playerOneRadio);
        playertwoRadio = view.findViewById(R.id.playerTwoRadio);
        playerTwo = view.findViewById(R.id.playerTwo);
        playerTwo.setBackgroundColor(0);
        playerOne.setBackgroundColor(getResources().getColor(R.color.playerBackground));
        playeroneRadio.setChecked(true);
        playertwoRadio.setChecked(false);
        dbHelper = new DbHelper(getActivity());
        sqLiteDatabase = dbHelper.getWritableDatabase();
        if (getArguments() != null) {

            currentMatchId = getArguments().getLong("id");
            Toast.makeText(getActivity(), "currentMatch" + currentMatchId, Toast.LENGTH_SHORT).show();

        }

        playerOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NameandIdModel nameandIdModel = (NameandIdModel) playerOne.getTag();
                currentPlaying = nameandIdModel.getId();
                playerTwo.setBackgroundColor(0);
                playerOne.setBackgroundColor(getResources().getColor(R.color.playerBackground));
                playeroneRadio.setChecked(true);
                playertwoRadio.setChecked(false);
            }
        });

        playerTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NameandIdModel nameandIdModel = (NameandIdModel) playerOne.getTag();
                currentPlaying = nameandIdModel.getId();
                playerOne.setBackgroundColor(0);
                playerTwo.setBackgroundColor(getResources().getColor(R.color.playerBackground));
                playertwoRadio.setChecked(true);
                playeroneRadio.setChecked(false);
            }
        });
        thisOver = view.findViewById(R.id.thisOver);
        number7 = view.findViewById(R.id.number7);
        number5 = view.findViewById(R.id.number5);
        number6 = view.findViewById(R.id.number6);
        number4 = view.findViewById(R.id.number4);
        number3 = view.findViewById(R.id.number3);
        number2 = view.findViewById(R.id.number2);
        number0 = view.findViewById(R.id.number0);
        buttonUndo = view.findViewById(R.id.buttonUndo);
        numberLb = view.findViewById(R.id.numberLb);
        numberLb.setOnClickListener(this);
        numberBold = view.findViewById(R.id.numberBold);
        numberBold.setOnClickListener(this);
        numberNoBall = view.findViewById(R.id.numberNoBall);
        numberNoBall.setOnClickListener(this);
        numberWhite = view.findViewById(R.id.numberWhite);
        numberWhite.setOnClickListener(this);

        registerForContextMenu(dottedMenu);
        dottedMenu.setOnClickListener(this);
        number1 = view.findViewById(R.id.number1);
        numberW = view.findViewById(R.id.numberW);


        return view;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, v.getId(), 0, "Bowler Hurt");
        menu.add(0, v.getId(), 1, "Batsman Retired Hurt");
        menu.add(0, v.getId(), 2, "Change Match Details");
        menu.add(0, v.getId(), 3, "change Batsman");
        menu.add(0, v.getId(), 4, "Change Bowler");

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dottedMenu:
                final CharSequence[] options = {"Bowler Hurt", "Batsman Retired Hurt", "Change Match Details",
                        "change Batsman"};
                androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Match Type");

                builder.setItems(options, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (options[item].equals("Bowler Hurt")) {

                        } else if (options[item].equals("Batsman Retired Hurt")) {

                        } else if (options[item].equals("Change Match Details")) {

                        } else if (options[item].equals("change Batsman")) {

                        }

                    }
                });
                builder.show();
                break;
            case R.id.numberLb:
                final CharSequence[] numberLb = {"1lb", "2lb", "3lb",
                        "4lb", "5lb", "6lb", "7lb", "lb+wkt"};
                androidx.appcompat.app.AlertDialog.Builder builderNumberLb = new AlertDialog.Builder(getActivity());
                builderNumberLb.setTitle("Choose Match Type");

                builderNumberLb.setItems(numberLb, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (numberLb[item].equals("1lb")) {

                        } else if (numberLb[item].equals("2lb")) {

                        } else if (numberLb[item].equals("3lb")) {

                        } else if (numberLb[item].equals("4lb")) {

                        } else if (numberLb[item].equals("5lb")) {

                        } else if (numberLb[item].equals("6lb")) {

                        } else if (numberLb[item].equals("7lb")) {

                        } else if (numberLb[item].equals("lb+wkt")) {

                        }

                    }
                });
                builderNumberLb.show();
                break;
            case R.id.numberBold:
                final CharSequence[] numberBold = {"1b", "2b", "3b",
                        "4b", "5b", "6b", "7b", "b+wkt"};
                androidx.appcompat.app.AlertDialog.Builder builderNumberBold = new AlertDialog.Builder(getActivity());
                builderNumberBold.setTitle("Choose Match Type");

                builderNumberBold.setItems(numberBold, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (numberBold[item].equals("1b")) {

                        } else if (numberBold[item].equals("2b")) {

                        } else if (numberBold[item].equals("3b")) {

                        } else if (numberBold[item].equals("4b")) {

                        } else if (numberBold[item].equals("5b")) {

                        } else if (numberBold[item].equals("6b")) {

                        } else if (numberBold[item].equals("7b")) {

                        } else if (numberBold[item].equals("b+wkt")) {

                        }

                    }
                });
                builderNumberBold.show();
                break;
            case R.id.numberNoBall:
                final CharSequence[] numberNoBall = {"1nb", "2nb", "3nb",
                        "4nb", "5nb", "6nb", "7nb", "nb+wkt"};
                androidx.appcompat.app.AlertDialog.Builder builderNumberNoBall = new AlertDialog.Builder(getActivity());
                builderNumberNoBall.setTitle("Choose Match Type");

                builderNumberNoBall.setItems(numberNoBall, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (numberNoBall[item].equals("1nb")) {

                        } else if (numberNoBall[item].equals("2nb")) {

                        } else if (numberNoBall[item].equals("3nb")) {

                        } else if (numberNoBall[item].equals("4nb")) {

                        } else if (numberNoBall[item].equals("5nb")) {

                        } else if (numberNoBall[item].equals("6nb")) {

                        } else if (numberNoBall[item].equals("7nb")) {

                        } else if (numberNoBall[item].equals("nb+wkt")) {

                        }

                    }
                });
                builderNumberNoBall.show();
                break;
            case R.id.numberWhite:
                final CharSequence[] numberWhite = {"1wb", "2wb", "3wb",
                        "4wb", "5wb", "6wb", "7wb", "wb+wkt"};
                androidx.appcompat.app.AlertDialog.Builder builderNumberWhite = new AlertDialog.Builder(getActivity());
                builderNumberWhite.setTitle("Choose Match Type");

                builderNumberWhite.setItems(numberWhite, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (numberWhite[item].equals("1wb")) {

                        } else if (numberWhite[item].equals("2wb")) {

                        } else if (numberWhite[item].equals("3wb")) {

                        } else if (numberWhite[item].equals("4wb")) {

                        } else if (numberWhite[item].equals("5wb")) {

                        } else if (numberWhite[item].equals("6wb")) {

                        } else if (numberWhite[item].equals("7wb")) {

                        } else if (numberWhite[item].equals("wb+wkt")) {

                        }

                    }
                });
                builderNumberWhite.show();
                break;
        }
    }
}
