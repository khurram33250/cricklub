package com.cricklub.fragments;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.cricklub.CONSTANT;
import com.cricklub.LocalDb.DbHelper;
import com.cricklub.LocalDb.LocalDbConstant;
import com.cricklub.R;
import com.cricklub.activities.SelectPlayersActivity;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;


public class StartMatchFragment extends Fragment implements View.OnClickListener {
    public String TAG = "startMatchFragment";
    Button nextBtn;
    Intent intent;
    String tounamentSeries = "";
    TextInputLayout tournamentName, tournamentStage, battingTeam, bowlingTeam, overs, venue, noofplayersInput;
    TextInputEditText tournamentNameEdit, tournamentStageEdit, battingTeamEdit, bowlingTeamEdit, oversEdit, venueEdit, noofplayersEdit;
    String decisionValue = "Batting";
    long id;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferenceEditor;
    private RadioGroup chooseBatting;
    private RadioButton battingBtn, bowlingBtn;
    int matchState;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        View view = inflater.inflate(R.layout.fragment_start_match, container, false);

        nextBtn = view.findViewById(R.id.nextBtn);
        nextBtn.setOnClickListener(this);
        getActivity().setTitle("Start Match");
        tournamentName = view.findViewById(R.id.tounnamentInputLayout);
        tournamentStage = view.findViewById(R.id.tounamentStage);
        battingTeam = view.findViewById(R.id.battingTeam);
        bowlingTeam = view.findViewById(R.id.bowlingInput);
        venue = view.findViewById(R.id.venue);
        tournamentNameEdit = view.findViewById(R.id.tournamentEdittext);
        tournamentStageEdit = view.findViewById(R.id.tounamentStageEdit);
        battingTeamEdit = view.findViewById(R.id.battingTeamEdit);
        bowlingTeamEdit = view.findViewById(R.id.bowlTeamEdittext);
        venueEdit = view.findViewById(R.id.venueEdit);
        oversEdit = view.findViewById(R.id.oversEdit);
        overs = view.findViewById(R.id.overs);
        chooseBatting = view.findViewById(R.id.chooseBatting);
        battingBtn = view.findViewById(R.id.batting);
        bowlingBtn = view.findViewById(R.id.bowling);
        noofplayersInput = view.findViewById(R.id.noofplayersInput);
        noofplayersEdit = view.findViewById(R.id.noofplayersEdit);

        chooseBatting.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton selectedBtn = radioGroup.findViewById(selectedId);
                if (selectedBtn.getText().equals("batting")) {
                    decisionValue = "Batting";
                } else {
                    decisionValue = "Bowling";
                }
            }
        });

        sharedPreferences = getActivity().getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        sharedPreferenceEditor = sharedPreferences.edit();
        registerForContextMenu(tournamentStageEdit);

        bowlingTeamEdit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    if (battingTeamEdit.getText().toString().length() > 0) {
                        sharedPreferenceEditor.putString(CONSTANT.BATTING_TEAM, battingTeamEdit.getText().toString());
                        sharedPreferenceEditor.commit();
                    }
                }

            }
        });


        tournamentStageEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] options = {"Test Match", "One Day Match"};
                androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Choose Match Type");

                builder.setItems(options, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int item) {

                        if (options[item].equals("Test Match")) {
                            tournamentStageEdit.setText("Test Match");
                            overs.setVisibility(View.GONE);
                        } else if (options[item].equals("One Day Match")) {
                            tournamentStageEdit.setText("One Day Match");
                            overs.setVisibility(View.VISIBLE);
                        }
                    }
                });
                builder.show();
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.nextBtn:
                if (buttonClickFunc(tournamentNameEdit.getText().toString(), tournamentStageEdit.getText().toString(), battingTeamEdit.getText().toString(),
                        bowlingTeamEdit.getText().toString(), decisionValue, venueEdit.getText().toString(), oversEdit.getText().toString(), noofplayersEdit.getText().toString())) {
//                    if (decisionValue.equalsIgnoreCase("Batting")) {
//                        intent = new Intent(getActivity(), StartMatchEnterPlayerDetail.class);
//                        intent.putExtra("matchId", id);
//                        intent.putExtra("type", CONSTANT.DECISION_VALUE_BATTING);
//                        startActivity(intent);
//                    } else {
//
//                    }

                    final CharSequence[] options = {"First Innings", "Second Innings"};
                    androidx.appcompat.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Choose Inning Type");

                    builder.setItems(options, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int item) {

                            if (options[item].equals("First Innings")) {
                                Intent intent = new Intent(getActivity(), SelectPlayersActivity.class);
                                intent.putExtra("PlayerCount", noofplayersEdit.getText().toString());
                                intent.putExtra("MatchId", id);
                                intent.putExtra("state",matchState);
                                getActivity().startActivity(intent);
                            } else if (options[item].equals("One Day Match")) {
                                Toast.makeText(getActivity(), "Second Innings", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    builder.show();


                }
                break;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();

        if (v.getId() == R.id.tounamentStageEdit) {
            inflater.inflate(R.menu.tounament, menu);
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.singleMatch:
                tounamentSeries = "Single Match";
                tournamentStageEdit.setText(tounamentSeries);
                tournamentStageEdit.setSelection(tounamentSeries.length());
                Log.d(TAG, tounamentSeries);
                //  tournamentStageEdit.setText(tounamentSeries.length());

                return true;
            case R.id.groupStage:
                tounamentSeries = "Group Stage";
             /*   tournamentStageEdit.setText(tounamentSeries);
                tournamentStageEdit.setSelection(tounamentSeries.length());*/

                return true;
            case R.id.quarterFinal:
                tounamentSeries = "Quarter Stage";
              /*  tournamentStageEdit.setText(tounamentSeries);
                tournamentStageEdit.setText(tounamentSeries.length());*/

                return true;
            case R.id.semiFinal:
                tounamentSeries = "Semi Final";
              /*  tournamentStageEdit.setText(tounamentSeries);
                tournamentStageEdit.setText(tounamentSeries.length());
*/
                return true;
            case R.id.finalM:
                tounamentSeries = "Final Match";
              /*  tournamentStageEdit.setText(tounamentSeries);
                tournamentStageEdit.setText(tounamentSeries.length());*/

                return true;

        }
        return super.onContextItemSelected(item);
    }


    private boolean buttonClickFunc(String tounamentName, String tounamentStage, String battingTeamS, String BowlingTeam
            , String tossWon, final String venues, final String oversS, final String noofPlayers) {
        DbHelper dbHelper;
        SQLiteDatabase sqLiteDatabase;
        dbHelper = new DbHelper(getActivity());
        sqLiteDatabase = dbHelper.getWritableDatabase();
        final String tounamentname, tounamentstage, battingteam, bowlingteam, tosswons, venueName, oversName, totalplayers;
        tounamentname = tounamentName;
        tounamentstage = tounamentStage;
        battingteam = battingTeamS;
        bowlingteam = BowlingTeam;
        tosswons = tossWon;
        venueName = venues;
        oversName = oversS;
        totalplayers = noofPlayers;
        if (!(totalplayers.length() > 0)) {
            noofplayersInput.setError("No of Players is Required");
        }
        if (!(tounamentname.length() > 0)) {
            tournamentName.setError("Tournament Name is Required");
        }
        if (!(tounamentstage.length() > 0)) {
            tournamentStage.setError("Tournament Stage is Required");
        }
        if (!(battingTeamEdit.getText().toString().length() > 0)) {
            this.battingTeam.setError("Batting Team Name is Required");
        }
        if (!(bowlingTeamEdit.getText().toString().length() > 0)) {
            this.bowlingTeam.setError("Bowling Team Name is Required");
        }
        if (!(venueEdit.getText().toString().length() > 0)) {
            this.venue.setError("Venue Name is Required");
        }
        if (overs.getVisibility() == View.VISIBLE) {
            if (!(overs.getEditText().getText().toString().length() > 0)) {
                this.overs.setError("Overs is Required");
            }
        }
        noofplayersEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (noofplayersInput.getEditText().getText().toString().length() > 0) {
                    noofplayersInput.setError("");
                }
            }
        });


        this.tournamentNameEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (tounamentname.length() > 0) {
                    tournamentName.setError("");
                }
            }
        });
        this.tournamentStageEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (tournamentNameEdit.length() > 0) {
                    tournamentStage.setError("");
                }
            }
        });
        this.battingTeamEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (battingteam.length() > 0) {
                    battingTeam.setError("");
                }
            }
        });
        bowlingTeamEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (bowlingteam.length() > 0) {
                    bowlingTeam.setError("");
                }
            }
        });
        venueEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (venues.length() > 0) {
                    venue.setError("");
                }
            }
        });

        venueEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (venueEdit.getText().toString().length() > 0) {
                    venue.setError("");
                }
            }
        });

        oversEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (overs.getEditText().getText().toString().length() > 0) {
                    overs.setError("");
                }
            }
        });
        if (tounamentname.length() > 0 && tounamentstage.length() > 0 && battingteam.length() > 0 &&
                bowlingteam.length() > 0 && tosswons.length() > 0 && venueName.length() > 0 && totalplayers.length() > 0) {
            if (overs.getVisibility() == View.VISIBLE) {
                if (!(oversName.length() > 0)) {
                    Toast.makeText(getActivity(), "Please Enter Number of Overs", Toast.LENGTH_SHORT).show();
                    return false;
                }
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put(LocalDbConstant.MATCH_NAME, tounamentname);
            contentValues.put(LocalDbConstant.MATCH_STAGE, tounamentstage);
            contentValues.put(LocalDbConstant.BATTING_TEAM, battingteam);
            contentValues.put(LocalDbConstant.BOWLING_TEAM, bowlingteam);
            contentValues.put(LocalDbConstant.TOSS_WON_BY, tosswons);
            contentValues.put(LocalDbConstant.VENUE, venueName);
            contentValues.put(LocalDbConstant.TOTAL_OVERS, oversName);
            matchState = decisionValue.equalsIgnoreCase("Batting") ? 1 : 2;
            contentValues.put(LocalDbConstant.CURRENT_INNINGS, matchState);
            contentValues.put(LocalDbConstant.OPPORENT_SCORE, "");
            contentValues.put(LocalDbConstant.NO_OF_PLAYERS, totalplayers);
            contentValues.put(LocalDbConstant.CURRENT_DATE, System.currentTimeMillis() + "");

            id = sqLiteDatabase.insert(LocalDbConstant.START_MATCH, null, contentValues);
            if (id != -1) {

                Toast.makeText(getActivity(), "inserted" + id, Toast.LENGTH_SHORT).show();
                return true;
            }
        }
        return false;

    }


}

