package com.cricklub.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.adapters.HistoryFragmentAdapter;
import com.cricklub.R;

public class HistoryFragment extends Fragment {
    RecyclerView historyRecyclerView;
    HistoryFragmentAdapter historyFragmentAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        getActivity().setTitle("History");

        View view = inflater.inflate(R.layout.fragment_history, container, false);
        historyRecyclerView = view.findViewById(R.id.historyRecyclerView);
        historyFragmentAdapter=new HistoryFragmentAdapter(getActivity());
        historyRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        historyRecyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        historyRecyclerView.setAdapter(historyFragmentAdapter);


        return view;
    }


}
