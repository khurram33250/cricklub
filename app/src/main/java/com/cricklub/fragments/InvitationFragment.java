package com.cricklub.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.activities.InvitationAdapter;
import com.cricklub.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvitationFragment extends Fragment {
    RecyclerView recyclerView;
    InvitationAdapter invitationAdapter;

    public InvitationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_invitation, container, false);
        getActivity().setTitle("Invitation");

        recyclerView=view.findViewById(R.id.recyclerView);
        invitationAdapter=new InvitationAdapter(getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(invitationAdapter);
        return view;
    }

}
