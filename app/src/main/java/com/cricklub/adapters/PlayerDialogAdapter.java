package com.cricklub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.R;

public class PlayerDialogAdapter extends RecyclerView.Adapter<PlayerDialogAdapter.PlayerDialogViewHolder>{
    Context context;
    LayoutInflater layoutInflater;

    public PlayerDialogAdapter(Context context) {
        this.context = context;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public PlayerDialogViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=layoutInflater.inflate(R.layout.player_detail_view,parent,false);
        return new PlayerDialogViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerDialogViewHolder holder, int position) {
        holder.playerName.setText("Player "+position);

    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class PlayerDialogViewHolder extends RecyclerView.ViewHolder
    {
        public TextView playerName;
        public PlayerDialogViewHolder(@NonNull View itemView) {
            super(itemView);
            playerName=itemView.findViewById(R.id.playerName);
        }
    }
}
