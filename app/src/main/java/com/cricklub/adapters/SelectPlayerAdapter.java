package com.cricklub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.R;
import com.cricklub.models.SelectedPlayer;

import java.util.ArrayList;
import java.util.List;

public class SelectPlayerAdapter extends RecyclerView.Adapter<SelectPlayerAdapter.MyViewHolder> {

    private List<SelectedPlayer> playersList;
    private int totalPlayerCount = 0;
    private int selectedPlayerCount = 0;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RadioButton radioButtonName;

        public MyViewHolder(View view) {
            super(view);
            radioButtonName = view.findViewById(R.id.radioButtonName);
        }
    }

    public SelectPlayerAdapter(Context context, List<SelectedPlayer> playersList, int playerCount) {
        this.context = context;
        this.playersList = playersList;
        this.totalPlayerCount = playerCount;
    }

    @Override
    public SelectPlayerAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SelectPlayerAdapter.MyViewHolder holder, final int position) {

        final SelectedPlayer selectedPlayer = playersList.get(position);

        holder.radioButtonName.setText(selectedPlayer.getPlayerName());
        holder.radioButtonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SelectedPlayer player = playersList.get(position);
                if (!player.isSelected()) {
                    if (selectedPlayerCount < totalPlayerCount) {
                        holder.radioButtonName.setChecked(true);
                        playersList.get(position).setSelected(true);
                        selectedPlayerCount++;
                    } else {
                        holder.radioButtonName.setChecked(false);
                        Toast.makeText(context, "You Select only " + totalPlayerCount, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    holder.radioButtonName.setChecked(false);
                    playersList.get(position).setSelected(false);
                    selectedPlayerCount--;
                }
            }

        });

    }

    @Override
    public int getItemCount() {
        return playersList.size();
    }

    public List<String> getSelectedPlayers() {
        List<String> selectPlayers = new ArrayList<>();
        for (int i = 0; i < playersList.size(); i++) {
            if (playersList.get(i).isSelected()) {
                selectPlayers.add(playersList.get(i).getId());
            }
        }
        return selectPlayers;
    }
}