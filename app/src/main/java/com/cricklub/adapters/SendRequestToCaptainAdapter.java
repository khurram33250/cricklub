package com.cricklub.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.R;
import com.cricklub.models.SendRequest;

import java.util.List;

public class SendRequestToCaptainAdapter extends RecyclerView.Adapter<SendRequestToCaptainAdapter.MyViewHolder> {

    private List<SendRequest> playersList;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageViewProfile;
        public TextView textViewName, textViewCity, textViewCreatedAt;
        public ImageButton imageButtonTick;

        public MyViewHolder(View view) {
            super(view);

            imageViewProfile = view.findViewById(R.id.imageView_profile);
            textViewName = view.findViewById(R.id.textView_name);
            textViewCity = view.findViewById(R.id.textView_city);
            textViewCreatedAt = view.findViewById(R.id.textView_createdAt);
            imageButtonTick = view.findViewById(R.id.imageButton_tick);

        }
    }

    public SendRequestToCaptainAdapter(List<SendRequest> playersList) {
        this.playersList = playersList;
    }

    @Override
    public SendRequestToCaptainAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.send_request_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final SendRequestToCaptainAdapter.MyViewHolder holder, final int position) {
    }

    @Override
    public int getItemCount() {
        return playersList.size();
    }
}
