package com.cricklub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.models.PlayerModel;
import com.cricklub.R;

import java.util.List;

public class ShowPlayerForMatchAdapter extends RecyclerView.Adapter<ShowPlayerForMatchAdapter.ShowPlayerViewHolder>{
    Context context;
    LayoutInflater layoutInflater;
    List<PlayerModel> playerModels;

    public ShowPlayerForMatchAdapter(Context context, List<PlayerModel> playerModels) {
        this.context = context;
        this.playerModels = playerModels;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public ShowPlayerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=layoutInflater.inflate(R.layout.showplayer_view,parent,false);
        return new ShowPlayerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ShowPlayerViewHolder holder, int position) {
    holder.radioButton.setText(playerModels.get(position).name);
    }

    @Override
    public int getItemCount() {
        return playerModels.size();
    }

    public class ShowPlayerViewHolder extends RecyclerView.ViewHolder
    {
    public RadioButton radioButton;
        public ShowPlayerViewHolder(@NonNull View itemView) {
            super(itemView);
            radioButton=itemView.findViewById(R.id.playerName);
        }
    }
}
