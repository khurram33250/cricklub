package com.cricklub.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.R;
import com.cricklub.models.SelectedPlayer;

import java.util.ArrayList;
import java.util.List;

public class TeamManagementAdapter extends RecyclerView.Adapter<TeamManagementAdapter.TeamViewHolder> {

    Context context;
    LayoutInflater layoutInflater;
    private List<SelectedPlayer> playersList = new ArrayList<>();
    private RemovedPlayerListener removedPlayerListener;

    public void setOnPlayerRemoveListener(RemovedPlayerListener removedPlayerListener) {
        this.removedPlayerListener = removedPlayerListener;
    }

    public TeamManagementAdapter(Context context, List<SelectedPlayer> playersList) {
        this.context = context;
        this.playersList = playersList;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public TeamViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.team_management_view, parent, false);
        return new TeamViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TeamViewHolder holder, int position) {

        SelectedPlayer selectedPlayer = playersList.get(position);

        holder.playerName.setText(selectedPlayer.getPlayerName());
        holder.phoneNumber.setText(selectedPlayer.getPhone_no());
        if (selectedPlayer.getIs_app_user().equalsIgnoreCase("True")) {
            holder.cricklubUser.setText("CricketLub User");
        } else {
            holder.cricklubUser.setText("Not App User");
        }

        holder.imageButtonCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removedPlayerListener.onRemovePlayerClick();
            }
        });
    }

    @Override
    public int getItemCount() {
        return playersList.size();
    }

    public class TeamViewHolder extends RecyclerView.ViewHolder {

        ImageView profileIv;
        TextView playerName, phoneNumber, cricklubUser;
        ImageButton imageButtonCross;

        public TeamViewHolder(@NonNull View itemView) {
            super(itemView);

            profileIv = itemView.findViewById(R.id.profileIv);
            playerName = itemView.findViewById(R.id.playerName);
            phoneNumber = itemView.findViewById(R.id.phoneNumber);
            cricklubUser = itemView.findViewById(R.id.cricklub_user);
            imageButtonCross = itemView.findViewById(R.id.imageButton_cross);
        }
    }

    public interface RemovedPlayerListener {
        void onRemovePlayerClick();
    }
}
