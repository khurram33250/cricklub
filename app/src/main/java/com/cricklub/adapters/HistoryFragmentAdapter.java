package com.cricklub.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.activities.BattingAndBowlingDetailActivity;
import com.cricklub.R;

public class HistoryFragmentAdapter extends RecyclerView.Adapter<HistoryFragmentAdapter.HistoryViewHolder>{
    LayoutInflater layoutInflater;
    Context context;

    public HistoryFragmentAdapter(Context context) {
        this.context = context;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=layoutInflater.inflate(R.layout.history_fragment_view,parent,false);
        return new HistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {
    holder.itemView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent=new Intent(context, BattingAndBowlingDetailActivity.class);
            context.startActivity(intent);
        }
    });
    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class HistoryViewHolder extends RecyclerView.ViewHolder
    {
        public HistoryViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
