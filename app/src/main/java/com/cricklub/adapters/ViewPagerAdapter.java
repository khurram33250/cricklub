package com.cricklub.adapters;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.cricklub.fragments.BattingDetailFragment;
import com.cricklub.fragments.BowlingDetailFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        switch (position)
        {
            case 0:
                fragment=new BattingDetailFragment();
                return fragment;
            case 1:
                fragment=new BowlingDetailFragment();
                return fragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title=null;
        switch (position)
        {
            case 0:
                title="Your Batting";
                return title;
            case 1:
                title="Your Bowling";
                return title;

        }
        return super.getPageTitle(position);
    }
}
