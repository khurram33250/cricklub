package com.cricklub.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cricklub.AppUtils;
import com.cricklub.CONSTANT;
import com.cricklub.R;
import com.cricklub.WebReq.WebReqClass;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    TextInputLayout phoneInputLayout, emailInputLayout, passwordInputLayout,
            confirmPassInputLayout, nameInputLayout;
    Button signUpBtn;
    String phoneNo, email, password, confirmPassword, name;
    RadioGroup playerTypeRg;
    int playerType = 1;
    TextInputEditText phoneNoEt, emailEt, passwordEt, confirmPassEt, nameEt;
    private String TAG = "SignUpActivity";
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initViews();
    }

    private void initViews() {
        sharedPreferences = this.getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        phoneInputLayout = findViewById(R.id.phoneNoInputLayout);
        emailInputLayout = findViewById(R.id.emailInputLayout);
        passwordInputLayout = findViewById(R.id.passwordInputLayout);
        confirmPassInputLayout = findViewById(R.id.confirmPassInputLayout);
        nameInputLayout = findViewById(R.id.nameInputLayout);
        playerTypeRg = findViewById(R.id.playerTypeRg);
        signUpBtn = findViewById(R.id.signUpBtn);
        signUpBtn.setOnClickListener(this);
        phoneNoEt = findViewById(R.id.phoneNoEt);
        emailEt = findViewById(R.id.emailEt);
        passwordEt = findViewById(R.id.passwordEt);
        confirmPassEt = findViewById(R.id.confirmPasswordEt);
        nameEt = findViewById(R.id.nameET);
        playerTypeRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = findViewById(checkedId);
                if (radioButton.getText().equals("Captain")) {
                    playerType = 1;
                } else {
                    playerType = 2;
                }
            }
        });
        phoneNoEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (phoneInputLayout.getEditText().getText().toString().length() > 0) {
                    phoneInputLayout.setError("");
                }
            }
        });
        passwordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (passwordInputLayout.getEditText().getText().toString().length() > 0) {
                    passwordInputLayout.setError("");
                }
            }
        });
        confirmPassEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (confirmPassInputLayout.getEditText().getText().toString().length() > 0) {
                    confirmPassInputLayout.setError("");
                }
            }
        });
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        nameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (nameInputLayout.getEditText().getText().toString().length() > 0) {
                    nameInputLayout.setError("");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpBtn:
                phoneNo = phoneInputLayout.getEditText().getText().toString();
                email = emailInputLayout.getEditText().getText().toString();
                password = passwordInputLayout.getEditText().getText().toString();
                confirmPassword = confirmPassInputLayout.getEditText().getText().toString();
                name = nameInputLayout.getEditText().getText().toString();
                if (TextUtils.isEmpty(phoneNo)) {
                    phoneInputLayout.setError("Phone no is Required");
                }
                if (TextUtils.isEmpty(password)) {
                    passwordInputLayout.setError("Password is Required");
                }
                if (TextUtils.isEmpty(confirmPassword)) {
                    confirmPassInputLayout.setError("Confirm Password is Required");
                }
                if (TextUtils.isEmpty(name)) {
                    nameInputLayout.setError("Name is Required");
                }
                if (password.equalsIgnoreCase(confirmPassword)) {
                    if (phoneNo.length() > 0 && password.length() > 0 && confirmPassword.length() > 0 && name.length() > 0) {
                        if (AppUtils.isOnline(this)) {
                            signUpClick(phoneNo, password, confirmPassword, name, email);
                        } else {
                            Toast.makeText(this, "No Internet Found!", Toast.LENGTH_SHORT).show();
                        }
                    }
                } else {
                    Toast.makeText(this, "Password and Confirm Password must Match", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void signUpClick(String phoneNo, String password, String confirmPassword, String name, String email) {
        RequestParams requestParams = new RequestParams();
        requestParams.put("name", name);
        requestParams.put("email", email);
        requestParams.put("phone", phoneNo);
        requestParams.put("password", password);
        requestParams.put("player", playerType);
        Log.d(TAG, requestParams.toString());
        WebReqClass.getInstance().post(CONSTANT.SIGN_UP, requestParams, new SignUpApi());

    }

    public class SignUpApi extends JsonHttpResponseHandler {
        @Override
        public void onStart() {
            super.onStart();
            progressDialog.show();

        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            if (statusCode == 200) {
                try {
                    String status = response.getString(CONSTANT.API_STATUS);
                    String message = response.getString(CONSTANT.API_MESSAGE);
                    if (status.equalsIgnoreCase("201")) {
                        String playerId = response.getString("playerID");
                        editor.putString(CONSTANT.PLAYER_ID, playerId);
                        editor.commit();

                        Intent intent = new Intent(SignUpActivity.this, VerificationActivity.class);
                        startActivity(intent);
                        finish();
                        Toast.makeText(SignUpActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(SignUpActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(SignUpActivity.this, "" + getResources().getString(R.string.messageIssue), Toast.LENGTH_SHORT).show();
            }
////            if(statusCode==200)
////            {
////                try {
////                    String status=response.getString("status");
////                    if(status.equalsIgnoreCase("200")) {
////                        String message = response.getString("message");
////                        Toast.makeText(SignUpActivity.this, ""+message, Toast.LENGTH_SHORT).show();
////                        startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
////                    }else
////                    {
////                        JSONObject error=response.getJSONObject("errors").getJSONObject("password");
////                        String errorMessage= error.getString("message");
////                        Toast.makeText(SignUpActivity.this, ""+errorMessage, Toast.LENGTH_SHORT).show();
////                    }
////                } catch (JSONException e) {
////                    e.printStackTrace();
////                }
//
//            }else
//            {
//                Toast.makeText(SignUpActivity.this, "Some Think Wrong Please Try Again!", Toast.LENGTH_SHORT).show();
//            }
            Log.d(TAG, response.toString());
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
            Log.d(TAG, throwable.getLocalizedMessage());
        }


        @Override
        public void onFinish() {
            super.onFinish();
            progressDialog.dismiss();
        }
    }
}
