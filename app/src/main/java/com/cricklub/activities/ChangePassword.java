package com.cricklub.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cricklub.AppUtils;
import com.cricklub.CONSTANT;
import com.cricklub.R;
import com.cricklub.WebReq.WebReqClass;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ChangePassword extends AppCompatActivity implements View.OnClickListener {
Toolbar toolbar;
TextInputLayout oldPassword,newPassword,confirmPassword;
TextInputEditText oldPasswordEdit,newPasswordEdit,confirmPasswordEdit;
Button continueBtn;
String oldPasswordValue,newPasswordValue,confirmPasswordValue;
SharedPreferences sharedPreferences;
ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initViews();
    }
    private void initViews()
    {
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        oldPassword=findViewById(R.id.oldPassword);
        newPassword=findViewById(R.id.password);
        confirmPassword=findViewById(R.id.confirmPassword);
        oldPasswordEdit=findViewById(R.id.oldPasswordEdit);
        newPasswordEdit=findViewById(R.id.passwordEdit);
        confirmPasswordEdit=findViewById(R.id.confirmPasswordEdit);
        continueBtn=findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(this);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        sharedPreferences=this.getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        oldPasswordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            if(oldPassword.getEditText().getText().toString().length()>0)
            {
                oldPassword.setError("");
            }
            }
        });
        newPasswordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            if(newPassword.getEditText().getText().toString().length()>0)
            {
                newPassword.setError("");
            }
            }
        });
        confirmPasswordEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            if(confirmPassword.getEditText().getText().toString().length()>0)
            {
                confirmPassword.setError("");
            }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.continueBtn:
                oldPasswordValue=oldPassword.getEditText().getText().toString();
                newPasswordValue=newPassword.getEditText().getText().toString();
                confirmPasswordValue=confirmPassword.getEditText().getText().toString();
                if(!(oldPasswordValue.length()>0))
                {
                    oldPassword.setError("Old Password is required");
                }
                if(!(newPasswordValue.length()>0))
                {
                    newPassword.setError("New Password is Required");
                }
                if(!(confirmPasswordValue.length()>0))
                {
                    confirmPassword.setError("Confirm Password is Required");
                }
                if(oldPasswordValue.length()>0 && newPasswordValue.length()>0 && confirmPasswordValue.length()>0)
                {
                    if(newPasswordValue.equalsIgnoreCase(confirmPasswordValue))
                    {
                        if(AppUtils.isOnline(this)) {
                            RequestParams requestParams = new RequestParams();
                            requestParams.put("oldPassword", oldPasswordValue);
                            requestParams.put("password", newPasswordValue);
                            requestParams.put("confirmPassword", confirmPasswordValue);
                            requestParams.put("playerID", sharedPreferences.getString(CONSTANT.PLAYER_ID, ""));
                            WebReqClass.getInstance().post(CONSTANT.CHANGE_PASSWORD, requestParams, new ChangePasswordApi());
                        }else
                        {
                            Toast.makeText(this, "No Internet Found!", Toast.LENGTH_SHORT).show();
                        }
                    }else
                    {
                        confirmPassword.setError("Confirm Password Must be Same as PAssword");
                    }
                }
                break;
        }
    }
    public class ChangePasswordApi extends JsonHttpResponseHandler
    {
        @Override
        public void onStart() {
            super.onStart();
            progressDialog.show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            if(statusCode==200)
            {
                try {
                    String status=response.getString(CONSTANT.API_STATUS);
                    String message=response.getString(CONSTANT.API_MESSAGE);
                    if(status.equalsIgnoreCase("201"))
                    {
                        Toast.makeText(ChangePassword.this, ""+message, Toast.LENGTH_SHORT).show();
                    }else {
                        Toast.makeText(ChangePassword.this, ""+message, Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else {
                Toast.makeText(ChangePassword.this, ""+getResources().getString(R.string.messageIssue), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
            Toast.makeText(ChangePassword.this, ""+getResources().getString(R.string.messageIssue), Toast.LENGTH_SHORT).show();

            progressDialog.dismiss();
        }

        @Override
        public void onFinish() {
            super.onFinish();
            progressDialog.dismiss();
        }
    }
}
