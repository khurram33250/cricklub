package com.cricklub.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.cricklub.AppUtils;
import com.cricklub.CONSTANT;
import com.cricklub.R;
import com.cricklub.WebReq.WebReqClass;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Intent intent;
    private TextView signUpTv, forgotPassword;
    TextInputLayout phoneInputLayout, passwordInputLayout;
    Button loginBtn;
    String phoneNumber, password;
    TextInputEditText phoneEt, passwordEt;
    String TAG = "LoginActivity";
    ProgressDialog progressDialog;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferenceEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        signUpTv = findViewById(R.id.signUpTv);
        signUpTv.setOnClickListener(this);
        forgotPassword = findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(this);
        phoneInputLayout = findViewById(R.id.phoneNoEt);
        passwordInputLayout = findViewById(R.id.passwordInputLayout);
        loginBtn = findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);
        phoneEt = findViewById(R.id.phoneEt);
        passwordEt = findViewById(R.id.passwordEt);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        sharedPreferences = this.getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        sharedPreferenceEditor = sharedPreferences.edit();

        passwordEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (passwordInputLayout.getEditText().getText().toString().length() > 0) {
                    passwordInputLayout.setError("");
                }
            }
        });
        phoneEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (phoneInputLayout.getEditText().getText().toString().length() > 0) {
                    phoneInputLayout.setError("");
                }
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.signUpTv:
                intent = new Intent(this, SignUpActivity.class);
                startActivity(intent);
                finish();


                break;
            case R.id.forgotPassword:
                intent = new Intent(this, ForgotPassword.class);
                startActivity(intent);
                finish();

                break;
            case R.id.loginBtn:
                phoneNumber = phoneInputLayout.getEditText().getText().toString();
                password = passwordInputLayout.getEditText().getText().toString();
                if (TextUtils.isEmpty(phoneNumber)) {
                    phoneInputLayout.setError("Phone number is Required");
                }
                if (TextUtils.isEmpty(password)) {
                    passwordInputLayout.setError("Password is Required");
                }
                if (phoneNumber.length() > 0 && password.length() > 0) {
                    if (AppUtils.isOnline(this)) {
                        loginClick(phoneNumber, password);
                    } else {
                        Toast.makeText(this, "No Internet Found", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private void loginClick(String phoneNumber, String password) {

        RequestParams requestParams = new RequestParams();
        requestParams.put("phone", phoneNumber);
        requestParams.put("password", password);
        String token = returnAccessToken();
        if (token != null)
            requestParams.put("access_token", token);
        else {
            String againToken = returnAccessToken();
            requestParams.put("access_token", token);

        }
        WebReqClass.getInstance().post(CONSTANT.LOGIN_IN, requestParams, new LoginApi());


    }

    public class LoginApi extends JsonHttpResponseHandler {
        @Override
        public void onStart() {
            super.onStart();
            progressDialog.show();

        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            if (statusCode == 200) {
                try {
                    String status = response.getString(CONSTANT.API_STATUS);
                    String message = response.getString(CONSTANT.API_MESSAGE);
                    if (status.equalsIgnoreCase("201")) {
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                        JSONObject playerObject = response.getJSONObject("Data");
                        String name = playerObject.getString("name");
                        String uniquId = playerObject.getString("uniqueId");
                        String email = playerObject.getString("email");
                        String phoneNumber = playerObject.getString("phone");
                        String playerTypes = playerObject.getString("playerAs");
                        sharedPreferenceEditor.putString(CONSTANT.NAME, name);
                        sharedPreferenceEditor.putString(CONSTANT.UNIQUE_ID, uniquId);
                        sharedPreferenceEditor.putString(CONSTANT.EMAIL, email);
                        sharedPreferenceEditor.putString(CONSTANT.PHONE_NUMBER, phoneNumber);
                        if (playerTypes.equalsIgnoreCase("1")) {
                            sharedPreferenceEditor.putBoolean(CONSTANT.IS_CAPTAIN, true);
                        } else {
                            sharedPreferenceEditor.putBoolean(CONSTANT.IS_CAPTAIN, false);
                        }
                        sharedPreferenceEditor.putString(CONSTANT.APP_STATE, "2");

                        sharedPreferenceEditor.commit();
                        if (sharedPreferences.getBoolean(CONSTANT.IS_CAPTAIN, false)) {
                            Intent intent1 = new Intent(LoginActivity.this, CreateTeamActivity.class);
                            startActivity(intent1);
                            finish();
                        } else {
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        }

                    } else {
                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(LoginActivity.this, "" + getResources().getString(R.string.messageIssue), Toast.LENGTH_SHORT).show();
            }
            Log.d(TAG, response.toString());
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
            Log.d(TAG, throwable.getLocalizedMessage());
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
            super.onSuccess(statusCode, headers, response);
            if (statusCode == 200) {
                Toast.makeText(LoginActivity.this, "Login Successfully", Toast.LENGTH_SHORT).show();
                try {
                    JSONObject jsonArray = response.getJSONObject(0);
                    boolean isCaptain;
                    if (jsonArray.getString("playerAs").equalsIgnoreCase("1")) {
                        isCaptain = true;
                    } else {
                        isCaptain = false;
                    }
                    storeLoginDetail(jsonArray.getString("playerID"), jsonArray.getString("uniqueId"), jsonArray.getString("name"), jsonArray.getString("phone"), isCaptain, jsonArray.getString("confirmation_code"));
                    Log.e(TAG, jsonArray.getString("uniqueId") + "," + jsonArray.getString("name") + "," + jsonArray.getString("phone") + "," + jsonArray.getString("playerAs") + "," + jsonArray.getString("confirmation_code"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(new Intent(LoginActivity.this, VerificationActivity.class));
                finish();
            } else {
                Toast.makeText(LoginActivity.this, "", Toast.LENGTH_SHORT).show();
            }

        }

        @Override
        public void onFinish() {
            super.onFinish();
            progressDialog.dismiss();
        }
    }

    public void storeLoginDetail(String playerId, String uniqueId, String name, String phoneNumber, boolean isCaptain, String confirmationCode) {
        sharedPreferenceEditor.putString(CONSTANT.PLAYER_ID, playerId);
        sharedPreferenceEditor.putString(CONSTANT.UNIQUE_ID, uniqueId);
        sharedPreferenceEditor.putString(CONSTANT.NAME, name);
        sharedPreferenceEditor.putString(CONSTANT.PHONE_NUMBER, phoneNumber);
        sharedPreferenceEditor.putBoolean(CONSTANT.IS_CAPTAIN, isCaptain);
        sharedPreferenceEditor.putString(CONSTANT.CONFIRMATION_CODE, confirmationCode);
        sharedPreferenceEditor.commit();
    }

    String token = null;

    private String returnAccessToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("exception", "getInstanceId failed", task.getException());
                            token = null;
                            return;
                        }

                        // Get new Instance ID token
                        token = task.getResult().getToken();
                        Log.e("token", token);
                        // Log and toast
                    }
                });
        return token;
    }
}
