package com.cricklub.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cricklub.AppUtils;
import com.cricklub.CONSTANT;
import com.cricklub.R;
import com.cricklub.WebReq.WebReqClass;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {
    private Toolbar toolbar;
    private Button continueBtn;
    String TAG = "VerficationActivity";
    ProgressDialog progressDialog;
    String code;
    private SharedPreferences sharedPreferences;
    TextInputLayout verificationCode;
    TextInputEditText verificationEdittext;
    TextView resendCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        initViews();
    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        continueBtn = findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        sharedPreferences = this.getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        verificationCode = findViewById(R.id.verificationCode);
        verificationEdittext = findViewById(R.id.verificationEdit);
        resendCode = findViewById(R.id.resendCode);
        resendCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            if(AppUtils.isOnline(VerificationActivity.this))
            {
                RequestParams requestParams=new RequestParams();
                requestParams.put("playerID",sharedPreferences.getString(CONSTANT.PLAYER_ID,""));
                WebReqClass.get(CONSTANT.RESEND_CODE,requestParams,new ResendCode());
            }
            }
        });
        verificationEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (verificationCode.getEditText().getText().toString().length() > 0) {
                    verificationCode.setError("");
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.continueBtn:
                code = verificationCode.getEditText().getText().toString();
                if (code.length() > 0) {
                    if (AppUtils.isOnline(this)) {

                        RequestParams requestParams = new RequestParams();
                        requestParams.put("code", code);
                        requestParams.put("playerId", sharedPreferences.getString(CONSTANT.PLAYER_ID, ""));
                        WebReqClass.getInstance().post(CONSTANT.VERIFICATION, requestParams, new VerficationActivity());
                    } else {
                        Toast.makeText(this, "No Internet Found", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    verificationCode.setError("Verification Code is Required");
                }
                break;
        }
    }

    public class VerficationActivity extends JsonHttpResponseHandler {
        @Override
        public void onStart() {
            super.onStart();
            progressDialog.show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            if (statusCode == 200) {
                try {
                    String status = response.getString(CONSTANT.API_STATUS);
                    String message = response.getString(CONSTANT.API_MESSAGE);
                    if (status.equalsIgnoreCase("201")) {
                        Toast.makeText(VerificationActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(VerificationActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(VerificationActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(VerificationActivity.this, "" + getResources().getString(R.string.messageIssue), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
            progressDialog.dismiss();
        }

        @Override
        public void onFinish() {
            super.onFinish();
            progressDialog.dismiss();
        }
    }

    public class ResendCode extends JsonHttpResponseHandler {
        @Override
        public void onStart() {
            super.onStart();
            progressDialog.show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            try {
                String status = response.getString(CONSTANT.API_STATUS);
                String message = response.getString(CONSTANT.API_MESSAGE);
                if (status.equalsIgnoreCase("201")) {
                    Toast.makeText(VerificationActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(VerificationActivity.this, CreateTeamActivity.class));
                } else {
                    Toast.makeText(VerificationActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);

            progressDialog.dismiss();
            Toast.makeText(VerificationActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onFinish() {
            super.onFinish();
            progressDialog.dismiss();
        }
    }
}
