package com.cricklub.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;

import com.cricklub.CONSTANT;
import com.cricklub.fragments.HistoryFragment;
import com.cricklub.fragments.InvitationFragment;
import com.cricklub.fragments.ScoreBoardMainFragment;
import com.cricklub.fragments.StartMatchFragment;
import com.cricklub.fragments.TeamManagementFragment;
import com.cricklub.fragments.UpdateProfileFragment;
import com.cricklub.LocalDb.DbHelper;
import com.cricklub.R;
import com.facebook.stetho.Stetho;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    NavigationView navigationView;
    DrawerLayout drawerLayout;
    FloatingActionButton shareTeamLink;
    ActionBarDrawerToggle toggle;
    Toolbar toolbar;
    SQLiteOpenHelper sqLiteOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor sharedPreferenceEditor;
    private TextView nameTv, emailTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Stetho.initializeWithDefaults(this);


        toolbar = findViewById(R.id.toolbar);
        Stetho.initializeWithDefaults(this);
        nameTv = findViewById(R.id.nameTv);
        emailTv = findViewById(R.id.emailTv);

        sqLiteOpenHelper = new DbHelper(MainActivity.this);
        sqLiteDatabase = sqLiteOpenHelper.getWritableDatabase();
        sharedPreferences = this.getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        sharedPreferenceEditor = sharedPreferences.edit();
//        nameTv.setText(sharedPreferences.getString(CONSTANT.NAME,""));
//        emailTv.setText(sharedPreferences.getString(CONSTANT.EMAIL,""));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        shareTeamLink = findViewById(R.id.fab);
        shareTeamLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Invitation");
                alertDialog.setMessage("Share This Link to Invite Friends in your Team");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Share", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_SEND);
                        intent.setType("text/plain");
                        intent.putExtra(Intent.EXTRA_TEXT, "Abc");
                        startActivity(Intent.createChooser(intent, "Share"));
                    }
                });
                alertDialog.show();
            }
        });
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // You have to manually toggle drawer and set icon here
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else
                    drawerLayout.openDrawer((int) Gravity.START);
            }
        });
        if (getIntent().getExtras() != null) {
            if (getIntent().getExtras().getInt(CONSTANT.CURRENT_FRAGMENT, -1) == 1) {
                Bundle bundle = new Bundle();
                bundle.putLong("id", getIntent().getExtras().getLong("id"));
                Fragment fragment = new ScoreBoardMainFragment();
                fragment.setArguments(bundle);
                loadFragment(fragment);
            }
        } else {

            loadFragment(new UpdateProfileFragment());
        }
        //testing sripts
//        loadFragment(new ScoreBoardMainFragment());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        loadFragment(new UpdateProfileFragment());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.changePassword:
                startActivity(new Intent(MainActivity.this, ChangePassword.class));
                break;
            case R.id.contact_us:
                Intent intent = new Intent(MainActivity.this, ContactUsActivity.class);
                intent.putExtra("state", "1");
                startActivity(intent);
                break;
            case R.id.about_us:
                Intent intent1 = new Intent(MainActivity.this, ContactUsActivity.class);
                intent1.putExtra("state", "2");
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.edit_profile:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                loadFragment(new UpdateProfileFragment());
                return true;
            case R.id.invitations:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                loadFragment(new InvitationFragment());
                return true;
            case R.id.history:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                loadFragment(new HistoryFragment());
                return true;
            case R.id.management:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                loadFragment(new TeamManagementFragment());
                return true;
            case R.id.start_match:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                loadFragment(new StartMatchFragment());
                return true;
            case R.id.logOut:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Warning");
                alertDialog.setMessage("Are you Sure to Logout");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Logout", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        sharedPreferenceEditor.clear().commit();
                        finish();

                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                    }
                });
                alertDialog.show();
                return true;
            case R.id.share_ling:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=" + getPackageName());
                startActivity(Intent.createChooser(intent, "Share"));
                return true;
        }
        return false;
    }
}
