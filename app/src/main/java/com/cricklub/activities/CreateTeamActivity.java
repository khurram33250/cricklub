package com.cricklub.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.cricklub.AppUtils;
import com.cricklub.CONSTANT;
import com.cricklub.R;
import com.cricklub.RealPathUtil;
import com.cricklub.WebReq.WebReqClass;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;

public class CreateTeamActivity extends AppCompatActivity {
    private static final int EXTERNAL_STORAGE_PERMISSION_CONSTANT = 100;
    private static final int REQUEST_PERMISSION_SETTING = 101;
    Toolbar toolbar;
    ImageView teamLogo, personIv;
    TextInputLayout teamName, location;
    TextInputEditText teamNameEt, locationEt;
    Button submit;
    int LOGO_IMAGE = 1;
    String teamLogoPath, personPath;
    int PERSON = 2;
    private boolean sentToSettings = false;
    private SharedPreferences permissionStatus, per;
    String TAG = "CreateTeamActivity";
    ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_team);
        permissionStatus = getSharedPreferences("permissionStatus", MODE_PRIVATE);
        sharedPreferences = this.getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putString(CONSTANT.APP_STATE, "1");
        editor.commit();


        permissionDialog();
        initViews();

    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        teamLogo = findViewById(R.id.teamLogo);
        personIv = findViewById(R.id.person);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        teamLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //You already have the permission, just go ahead.
                String requiredPermission = "android.permission.READ_EXTERNAL_STORAGE";
                int checkVal = checkCallingOrSelfPermission(requiredPermission);
                if (checkVal == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, LOGO_IMAGE);
                } else {
                    permissionDialog();
                }

            }
        });
        personIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String requiredPermission = "android.permission.READ_EXTERNAL_STORAGE";
                int checkVal = checkCallingOrSelfPermission(requiredPermission);
                if (checkVal == PackageManager.PERMISSION_GRANTED) {
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                    startActivityForResult(i, PERSON);
                } else {
                    permissionDialog();
                }
            }
        });
        teamName = findViewById(R.id.teamName);
        location = findViewById(R.id.location);
        teamNameEt = findViewById(R.id.teamNameEt);
        locationEt = findViewById(R.id.locationEt);
        locationEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (locationEt.getText().toString().length() > 0) {
                    location.setError("");
                }
            }
        });
        teamNameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (teamNameEt.getText().toString().length() > 0) {
                    teamName.setError("");
                }
            }
        });
        submit = findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //for testing purpose
                Intent intent=new Intent(CreateTeamActivity.this,MainActivity.class);
                startActivity(intent);
              /*  if (AppUtils.isOnline(CreateTeamActivity.this)) {
                    if (!(teamNameEt.getText().toString().length() > 0)) {
                        teamName.setError("Team Name is Require");
                    }
                    if (!(locationEt.getText().toString().length() > 0)) {
                        location.setError("Location is Required");
                    }
                    if (teamNameEt.getText().toString().length() > 0 && locationEt.getText().toString().length() > 0) {
                        RequestParams requestParams = new RequestParams();
                        requestParams.put("teamname", teamNameEt.getText().toString());
                        requestParams.put("cityname", locationEt.getText().toString());
                        requestParams.put("teamID", sharedPreferences.getString(CONSTANT.PLAYER_ID, ""));
                        try {
                            if (teamLogoPath != null)
                                requestParams.put("teamlogo", new File(teamLogoPath));
                            if (personPath != null)
                                requestParams.put("team_profile_image", new File(personPath));
                            WebReqClass.post(CONSTANT.UPDATE_TEAM_NAME, requestParams, new UpdateTeamName());
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                        Log.d(TAG, teamLogoPath + ",,," + personPath);
                    }
                } else {
                    Toast.makeText(CreateTeamActivity.this, "No Internet Found!", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_CONSTANT) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //The External Storage Write Permission is granted to you... Continue your left job...
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(CreateTeamActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    //Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(CreateTeamActivity.this);
                    builder.setTitle("Need Storage Permission");
                    builder.setMessage("This app needs storage permission");
                    builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();


                            ActivityCompat.requestPermissions(CreateTeamActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);


                        }
                    });
                    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                } else {
                    Toast.makeText(getBaseContext(), "Unable to get Permission", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOGO_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            teamLogo.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            teamLogoPath = RealPathUtil.getRealPath(CreateTeamActivity.this, data.getData());

        }
        if (requestCode == PERSON && resultCode == RESULT_OK && null != data) {
            Uri selectImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            personIv.setImageBitmap(BitmapFactory.decodeFile(picturePath));

            personPath = RealPathUtil.getRealPath(CreateTeamActivity.this, data.getData());
        }
    }

    private void permissionDialog() {
        if (ActivityCompat.checkSelfPermission(CreateTeamActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(CreateTeamActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Show Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateTeamActivity.this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        ActivityCompat.requestPermissions(CreateTeamActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else if (permissionStatus.getBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, false)) {
                //Previously Permission Request was cancelled with 'Dont Ask Again',
                // Redirect to Settings after showing Information about why you need the permission
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateTeamActivity.this);
                builder.setTitle("Need Storage Permission");
                builder.setMessage("This app needs storage permission.");
                builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        sentToSettings = true;
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, REQUEST_PERMISSION_SETTING);
                        Toast.makeText(getBaseContext(), "Go to Permissions to Grant Storage", Toast.LENGTH_LONG).show();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            } else {
                //just request the permission
                ActivityCompat.requestPermissions(CreateTeamActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, EXTERNAL_STORAGE_PERMISSION_CONSTANT);
            }

            SharedPreferences.Editor editor = permissionStatus.edit();
            editor.putBoolean(Manifest.permission.READ_EXTERNAL_STORAGE, true);
            editor.commit();


        }

    }

    public class UpdateTeamName extends JsonHttpResponseHandler {
        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            Log.e("response", response.toString());
            Toast.makeText(CreateTeamActivity.this, "" + response, Toast.LENGTH_SHORT).show();

            try {

                Log.e("response", response.toString());
                String status = response.getString("status");
                String message = response.getString("message");
                if (status.equalsIgnoreCase("201")) {
                    Toast.makeText(CreateTeamActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    JSONArray data = response.getJSONArray("Data");
                    editor.putString(CONSTANT.TEAM_ID, data.getJSONObject(0).getString("teamID"));
                    editor.putString(CONSTANT.TEAM_NAME, data.getJSONObject(0).getString("name"));
                    editor.putString(CONSTANT.TEAM_CITY, data.getJSONObject(0).getString("city"));
                    editor.putString(CONSTANT.TEAM_LOGO, data.getJSONObject(0).getString("logo"));
                    editor.putString(CONSTANT.TEAM_PROFILE, data.getJSONObject(0).getString("profileImage"));
                    editor.putString(CONSTANT.APP_STATE, "2");
                    editor.commit();
                    Intent intent = new Intent(CreateTeamActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();


                } else {
                    Toast.makeText(CreateTeamActivity.this, "" + message, Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
            super.onSuccess(statusCode, headers, response);
            Log.e("response", response.toString());
            Toast.makeText(CreateTeamActivity.this, "" + response, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStart() {
            super.onStart();
            progressDialog.show();
        }

        @Override
        public void onFinish() {
            super.onFinish();
            progressDialog.dismiss();
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
            Toast.makeText(CreateTeamActivity.this, "" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
