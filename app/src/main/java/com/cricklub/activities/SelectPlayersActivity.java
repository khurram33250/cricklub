package com.cricklub.activities;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.LocalDb.DbHelper;
import com.cricklub.LocalDb.LocalDbConstant;
import com.cricklub.R;
import com.cricklub.adapters.SelectPlayerAdapter;
import com.cricklub.models.SelectedPlayer;

import java.util.ArrayList;
import java.util.List;

public class SelectPlayersActivity extends AppCompatActivity {

    private Context context;
    private DbHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;
    private String playerTableName;
    private List<SelectedPlayer> playersList;
    private List<SelectedPlayer> selectedPlayerList;
    private RecyclerView recyclerViewSelectPlayer;
    private SelectPlayerAdapter selectPlayerAdapter;
    private int playerCount = -1;
    private long matchId;
    private List<String> playersID;
    private Toolbar toolbar;
    int matchState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_players);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        context = this;
        playersList = new ArrayList<>();
        selectedPlayerList = new ArrayList<>();

        Intent intent = getIntent();
        if (intent != null) {
            String str = getIntent().getExtras().getString("PlayerCount");
            if (str != null) {
                playerCount = Integer.valueOf(str);
            }
            matchId = getIntent().getExtras().getLong("MatchId");
            matchState = getIntent().getExtras().getInt("state");
        }

        recyclerViewSelectPlayer = findViewById(R.id.recyclerViewSelectPlayer);

        selectPlayerAdapter = new SelectPlayerAdapter(context, playersList, playerCount);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewSelectPlayer.setLayoutManager(mLayoutManager);
        recyclerViewSelectPlayer.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSelectPlayer.setAdapter(selectPlayerAdapter);

        playerTableName = LocalDbConstant.PLAYER_TABLE;

        dbHelper = new DbHelper(context);
        sqLiteDatabase = dbHelper.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + playerTableName, null);

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    String id = cursor.getString(cursor.getColumnIndex(LocalDbConstant.PLAYER_ID));
                    String name = cursor.getString(cursor.getColumnIndex(LocalDbConstant.PLAYER_NAME));

                    playersList.add(new SelectedPlayer(id, name, false));
                    cursor.moveToNext();
                }
            }
            selectPlayerAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_player_selection, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        if (item.getItemId() == R.id.action_done) {
            selectedPlayerList.clear();
            List<String> selectedIds = selectPlayerAdapter.getSelectedPlayers();
            for (int i = 0; i < selectedIds.size(); i++) {
                for (int j = 0; j < playersList.size(); j++)
                    if (selectedIds.get(i).equalsIgnoreCase(playersList.get(j).getId())) {
                        selectedPlayerList.add(playersList.get(j));
                    }
            }

            savePlayersDataToDB();
            return true;
        } else if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void savePlayersDataToDB() {
        playersID = new ArrayList<>();
        for (int i = 0; i < selectedPlayerList.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(LocalDbConstant.MATCH_ID_FOR_MATCH, matchId);
            contentValues.put(LocalDbConstant.PLAYER_ID_FOR_MATCH, selectedPlayerList.get(i).getId());
            contentValues.put(LocalDbConstant.SELECTED_PLAYER_STATE, "0"); // 0 means NotOut
            contentValues.put(LocalDbConstant.SELECTED_CREATED_AT, System.currentTimeMillis() + "");
            long id = sqLiteDatabase.insert(LocalDbConstant.SELECTED_PLAYER_TABLE, null, contentValues);
            playersID.add(id + "");
        }

        Intent intent = new Intent(context, StartMatchEnterPlayerDetail.class);
        intent.putExtra("MatchId", matchId);
        startActivity(intent);
    }

    // testing for git
}
