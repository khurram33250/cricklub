package com.cricklub.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cricklub.LocalDb.DbHelper;
import com.cricklub.LocalDb.LocalDbConstant;
import com.cricklub.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

public class AddPlayerManual extends AppCompatActivity {
    Toolbar toolbar;
    TextInputLayout name, email, phoneNo, player_cat_input;
    TextInputEditText nameEt, emailEt, phoneNoEt, player_cat_edit;
    Button addPlayer;
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_player_manual);
        initViews();
    }

    private void initViews() {
        dbHelper = new DbHelper(this);
        sqLiteDatabase = dbHelper.getWritableDatabase();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        name = findViewById(R.id.nameInputLayout);
        email = findViewById(R.id.emailInputLayout);
        phoneNo = findViewById(R.id.phoneNoInputLayout);
        nameEt = findViewById(R.id.nameET);
        emailEt = findViewById(R.id.emailEt);
        phoneNoEt = findViewById(R.id.phoneNoEt);
        player_cat_edit = findViewById(R.id.player_cat_edit);
        player_cat_input = findViewById(R.id.player_cat_input);
        player_cat_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (player_cat_edit.getText().toString().length() > 0) {
                    player_cat_input.setError("");
                }
            }
        });
        nameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (nameEt.getText().toString().length() > 0) {
                    name.setError("");
                }
            }
        });
        phoneNoEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (phoneNoEt.getText().toString().length() > 0) {
                    phoneNo.setError("");
                }
            }
        });
        addPlayer = findViewById(R.id.addPlayer);
        addPlayer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(name.getEditText().getText().toString().length() > 0)) {
                    name.setError("");
                }
                if (!(phoneNo.getEditText().getText().toString().length() > 0)) {
                    phoneNo.setError("Phone number is Required");
                }
                if (!(player_cat_edit.getText().toString().length() > 0)) {
                    player_cat_input.setError("Player category is Required");
                }
                if (name.getEditText().getText().toString().length() > 0 && phoneNo.getEditText().getText().toString().length() > 0 && player_cat_edit.getText().toString().length() > 0) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(LocalDbConstant.PLAYER_NAME, name.getEditText().getText().toString());
                    contentValues.put(LocalDbConstant.PLAYER_EMAIL, email.getEditText().getText().toString());
                    contentValues.put(LocalDbConstant.PHONE_NO, phoneNo.getEditText().getText().toString());
                    contentValues.put(LocalDbConstant.IS_APP_USER, "false");
                    contentValues.put(LocalDbConstant.PLAYER_CATEGORY, player_cat_edit.getText().toString());
                    if (sqLiteDatabase.insert(LocalDbConstant.PLAYER_TABLE, null, contentValues) != -1) {
                        Toast.makeText(AddPlayerManual.this, "Player Add Successfully", Toast.LENGTH_SHORT).show();
                        name.getEditText().setText("");
                        phoneNo.getEditText().setText("");
                        email.getEditText().setText("");
                        player_cat_input.getEditText().setText("");
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
