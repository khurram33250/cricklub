package com.cricklub.activities;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.LocalDb.DbHelper;
import com.cricklub.LocalDb.LocalDbConstant;
import com.cricklub.R;
import com.cricklub.RecyclerTouchListener;
import com.cricklub.adapters.ShowPlayerForMatchAdapter;
import com.cricklub.fragments.SimpleDividerItemDecoration;
import com.cricklub.models.PlayerModel;

import java.util.ArrayList;
import java.util.List;

public class ShowPlayerActivityForMatch extends AppCompatActivity {
    Toolbar toolbar;
    RecyclerView recyclerView;
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    List<PlayerModel> playerModels;
    ShowPlayerForMatchAdapter showPlayerForMatchAdapter;
    private String TAG = "ShowPlayerActivityForMatch";
    long matchId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_player_for_match);
        initViews();
    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        dbHelper = new DbHelper(this);
        sqLiteDatabase = dbHelper.getReadableDatabase();
        playerModels = new ArrayList<>();
        showPlayerForMatchAdapter = new ShowPlayerForMatchAdapter(this, playerModels);
        if (getIntent() != null) {
            matchId = getIntent().getExtras().getLong("MatchId");
        }

        Cursor cursor = sqLiteDatabase.rawQuery("Select * From " + LocalDbConstant.SELECTED_PLAYER_TABLE + " WHERE " + LocalDbConstant.MATCH_ID_FOR_MATCH + "='" + matchId + "' AND " + LocalDbConstant.SELECTED_PLAYER_STATE + "='0'", null);

        Log.d(TAG, cursor.getCount() + "..record size");
        if (cursor != null && cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    String playerId=cursor.getString(cursor.getColumnIndex(LocalDbConstant.PLAYER_ID_FOR_MATCH));
                    Cursor playerRecord=sqLiteDatabase.rawQuery("Select * From "+LocalDbConstant.PLAYER_TABLE+" WHERE "+LocalDbConstant.PLAYER_ID+"='"+playerId+"' limit 1",null);
                    if(playerRecord.moveToFirst());
                    {
                    PlayerModel playerModel = new PlayerModel();
                    playerModel.setId(playerRecord.getString(playerRecord.getColumnIndex(LocalDbConstant.PLAYER_ID)));
                    playerModel.setName(playerRecord.getString(playerRecord.getColumnIndex(LocalDbConstant.PLAYER_NAME)));
                    playerModel.setAppUser(playerRecord.getString(playerRecord.getColumnIndex(LocalDbConstant.IS_APP_USER)));
                    playerModel.setPhoneNumber(playerRecord.getString(playerRecord.getColumnIndex(LocalDbConstant.PHONE_NO)));
                    playerModel.setEmail(playerRecord.getString(playerRecord.getColumnIndex(LocalDbConstant.PLAYER_EMAIL)));
                    playerModels.add(playerModel);
                    }
                } while (cursor.moveToNext());
            }
        }
        recyclerView = findViewById(R.id.showPlayerActivity);
        recyclerView.setLayoutManager(new LinearLayoutManager(ShowPlayerActivityForMatch.this, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(ShowPlayerActivityForMatch.this));
        showPlayerForMatchAdapter.notifyDataSetChanged();
        recyclerView.setAdapter(showPlayerForMatchAdapter);
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent();
                intent.putExtra("id", playerModels.get(position).id);
                intent.putExtra("name", playerModels.get(position).name);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
