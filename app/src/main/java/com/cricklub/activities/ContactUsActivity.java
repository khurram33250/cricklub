package com.cricklub.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.cricklub.R;

public class ContactUsActivity extends AppCompatActivity {
Toolbar toolbar;
WebView webView;
String state;
ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        webView=findViewById(R.id.webView);
        progressDialog=new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        progressDialog.show();
        state=getIntent().getExtras().getString("state");
     webView.setWebViewClient(new WebViewClient()
     {
         @Override
         public void onPageFinished(WebView view, String url) {
             super.onPageFinished(view, url);
             progressDialog.dismiss();

         }
     });
        if(state.equalsIgnoreCase("1"))
        {
            webView.loadUrl(getResources().getString(R.string.contact_us));
        }else {
            webView.loadUrl(getResources().getString(R.string.about_us));
        }
        webView.getSettings().setJavaScriptEnabled(true);



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
