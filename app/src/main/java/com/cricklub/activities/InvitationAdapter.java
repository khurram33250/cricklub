package com.cricklub.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.R;

public class InvitationAdapter extends RecyclerView.Adapter<InvitationAdapter.InvitationViewHolder> {
    LayoutInflater layoutInflater;
    Context context;

    public InvitationAdapter(Context context) {
        this.context = context;
        layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public InvitationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=layoutInflater.inflate(R.layout.recyclerview_item,parent,false);
        return new InvitationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InvitationViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

    public class InvitationViewHolder extends RecyclerView.ViewHolder
    {

        public InvitationViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
