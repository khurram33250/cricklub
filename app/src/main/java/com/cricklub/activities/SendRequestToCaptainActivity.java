package com.cricklub.activities;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cricklub.R;
import com.cricklub.adapters.SendRequestToCaptainAdapter;
import com.cricklub.models.SendRequest;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;

public class SendRequestToCaptainActivity extends AppCompatActivity {

    private Context context;
    private Toolbar toolbar;
    private RecyclerView recyclerViewSendRequest;
    private SendRequestToCaptainAdapter sendRequestToCaptainAdapter;
    List<SendRequest> sendRequestList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_request_to_captain);

        context = this;
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sendRequestList = new ArrayList<>();

        sendRequestToCaptainAdapter = new SendRequestToCaptainAdapter(sendRequestList);
        recyclerViewSendRequest = findViewById(R.id.recyclerView_sendRequest);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewSendRequest.setLayoutManager(mLayoutManager);
        recyclerViewSendRequest.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSendRequest.setAdapter(sendRequestToCaptainAdapter);

    }

    public class SendRequestApi extends JsonHttpResponseHandler {
        @Override
        public void onStart() {
            super.onStart();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
            super.onSuccess(statusCode, headers, response);
            for (int i = 0; i < response.length(); i++) {
                try {
                    JSONObject jsonObject = response.getJSONObject(i);
                    SendRequest sendRequest = new SendRequest(jsonObject.getString("teamID"), jsonObject.getString("teamUnique"), jsonObject.getString("name"), jsonObject.getString("city"), jsonObject.getString("logo"), jsonObject.getString("profileImage"), jsonObject.getString("created_at"));
                    sendRequestList.add(sendRequest);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            sendRequestToCaptainAdapter.notifyDataSetChanged();

        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
        }

        @Override
        public void onFinish() {
            super.onFinish();
        }
    }
}
