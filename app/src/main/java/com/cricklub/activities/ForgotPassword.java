package com.cricklub.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cricklub.CONSTANT;
import com.cricklub.R;
import com.cricklub.WebReq.WebReqClass;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class ForgotPassword extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    private Button continueBtn;
    private TextInputLayout phoneNo;
    private TextInputEditText textInputEditText;
    ProgressDialog progressDialog;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initViews();
    }

    private void initViews() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        continueBtn = findViewById(R.id.continueBtn);
        continueBtn.setOnClickListener(this);
        phoneNo = findViewById(R.id.phoneNoEt);
        textInputEditText = findViewById(R.id.phoneNoEdit);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading....");
        progressDialog.setCancelable(false);
        sharedPreferences = this.getSharedPreferences(CONSTANT.PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        textInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (phoneNo.getEditText().getText().toString().length() > 0) {
                    phoneNo.setError("");
                }
            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueBtn:
                if (phoneNo.getEditText().getText().toString().length() > 0) {
                    RequestParams requestParams = new RequestParams();
                    requestParams.put("phone", phoneNo.getEditText().getText().toString());
                    WebReqClass.getInstance().post(CONSTANT.FORGOT_API_NO, requestParams, new ForgotPassWordApi());
                } else {
                    phoneNo.setError("Phone No is Required");
                }

                break;
        }
    }

    public class ForgotPassWordApi extends JsonHttpResponseHandler {
        @Override
        public void onStart() {
            super.onStart();
            progressDialog.show();
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
            super.onSuccess(statusCode, headers, response);
            if (statusCode == 200) {
                try {
                    String TAG="ForgotPassword";
                    Log.d(TAG,response.toString());
                    String status = response.getString(CONSTANT.API_STATUS);
                    String message = response.getString(CONSTANT.API_MESSAGE);
                    if (status.equalsIgnoreCase("201")) {
                        Toast.makeText(ForgotPassword.this, "" + message, Toast.LENGTH_SHORT).show();
                        JSONObject data = response.getJSONObject("Data");
                        String playerId = data.getString("playerID");
                        editor.putString(CONSTANT.PLAYER_ID, playerId);
                        editor.commit();
                        Intent intent = new Intent(ForgotPassword.this, ForgotPasswordSetPass.class);
                        startActivity(intent);
                    } else {
                        Toast.makeText(ForgotPassword.this, "" + message, Toast.LENGTH_SHORT).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(ForgotPassword.this, "" + getResources().getString(R.string.messageIssue), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
            super.onSuccess(statusCode, headers, response);
            if (statusCode == 200) {

            } else {
                Toast.makeText(ForgotPassword.this, "Failed to Proceed", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
            super.onFailure(statusCode, headers, responseString, throwable);
            progressDialog.dismiss();
        }

        @Override
        public void onFinish() {
            super.onFinish();
            progressDialog.dismiss();
        }
    }
}
