package com.cricklub.activities;

import android.content.Intent;
import android.os.Bundle;

import com.cricklub.R;

import android.view.Gravity;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.widget.TextView;

public class ScoreBoardNavigation extends AppCompatActivity implements View.OnClickListener {
NavigationView navigationView;
DrawerLayout drawerLayout;
Toolbar toolbar;
ActionBarDrawerToggle actionBarDrawerToggle;
TextView battingScoreBoard,bowlingScoreBoard,overScoreBoard,fallofWricket;
Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_board_navigation);
        toolbar=findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        actionBarDrawerToggle.syncState();
        battingScoreBoard=findViewById(R.id.battingScoreBoard);
        bowlingScoreBoard=findViewById(R.id.bowlingScoreBoard);
        bowlingScoreBoard.setOnClickListener(this);
        battingScoreBoard.setOnClickListener(this);
        overScoreBoard=findViewById(R.id.overScoreBoard);
        overScoreBoard.setOnClickListener(this);
        fallofWricket=findViewById(R.id.fallofWricket);
        fallofWricket.setOnClickListener(this);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // You have to manually toggle drawer and set icon here
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else
                    drawerLayout.openDrawer((int) Gravity.START);
            }
        });
//        loadFragment(new ScoreBoardMainFragment());

    }
    private void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.battingScoreBoard:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else
                    drawerLayout.openDrawer((int) Gravity.START);
                intent=new Intent(ScoreBoardNavigation.this,BattingScoreBoard.class);
                startActivity(intent);
                break;
            case R.id.bowlingScoreBoard:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else
                    drawerLayout.openDrawer((int) Gravity.START);
                intent=new Intent(ScoreBoardNavigation.this,BowlingScoreBoard.class);
                startActivity(intent);
                break;
            case R.id.overScoreBoard:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else
                    drawerLayout.openDrawer((int) Gravity.START);
                intent=new Intent(ScoreBoardNavigation.this,OverScoreBoard.class);
                startActivity(intent);
                break;
            case R.id.fallofWricket:
                if (drawerLayout.isDrawerOpen(GravityCompat.START))
                    drawerLayout.closeDrawer(GravityCompat.START);
                else
                    drawerLayout.openDrawer((int) Gravity.START);
                intent=new Intent(ScoreBoardNavigation.this,FallofWricket.class);
                startActivity(intent);
                break;
        }
    }
}
