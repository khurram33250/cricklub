package com.cricklub.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.cricklub.CONSTANT;
import com.cricklub.LocalDb.DbHelper;
import com.cricklub.LocalDb.LocalDbConstant;
import com.cricklub.models.NameandIdModel;
import com.cricklub.R;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

public class StartMatchEnterPlayerDetail extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    Button nextBtn;
    LayoutInflater layoutInflater;
    EditText playerFirst, playerSecond;
    TextInputLayout playerFirstInput, playerSecondInput;
    AlertDialog alertDialog;
    View view;
    List<NameandIdModel> nameandIdModelList;
    public long matchId;
    String state;
    DbHelper dbHelper;
    SQLiteDatabase sqLiteDatabase;
    String TAG = "StartMatchEnterPlayerDetail";
    String innnings = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_match_enter_player_detail);
        initiViews();
        if (getIntent().getExtras() != null) {
            matchId = getIntent().getExtras().getLong("MatchId");
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void initiViews() {
        dbHelper = new DbHelper(this);
        sqLiteDatabase = dbHelper.getWritableDatabase();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        nextBtn = findViewById(R.id.nextBtn);
        nextBtn.setOnClickListener(this);
        playerFirst = findViewById(R.id.playerFirst);
        nameandIdModelList = new ArrayList<>();
        playerSecond = findViewById(R.id.secondPlayer);
        playerFirst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartMatchEnterPlayerDetail.this, ShowPlayerActivityForMatch.class);
                intent.putExtra("MatchId", matchId);
                startActivityForResult(intent, 100);
                Toast.makeText(StartMatchEnterPlayerDetail.this, "called", Toast.LENGTH_SHORT).show();
            }
        });
        playerFirstInput = findViewById(R.id.firstBatsMen);
        playerSecondInput = findViewById(R.id.secondBatsMen);
        playerSecond.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(StartMatchEnterPlayerDetail.this, ShowPlayerActivityForMatch.class);
                intent.putExtra("MatchId", matchId);

                startActivityForResult(intent, 101);

            }
        });
        layoutInflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = layoutInflater.inflate(R.layout.playerdetaildialog, null);
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setView(view);

        playerFirst.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });
        playerSecond.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        List<String> playersId = new ArrayList<>();
        playersId.add(getIntent().getExtras().getString("playerid1"));
        playersId.add(getIntent().getExtras().getString("playerid2"));
        int count = 0;
        switch (v.getId()) {
            case R.id.nextBtn:
                if (!(playerFirst.getText().toString().equalsIgnoreCase(playerSecond.getText().toString()))) {
                    for (NameandIdModel nameandIdModel : nameandIdModelList) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(LocalDbConstant.MATCH_PLAYER_INNINGS, innnings);
                        contentValues.put(LocalDbConstant.MATCH_PLAYER_STATUS, "1");//batting
                        contentValues.put(LocalDbConstant.MATCH_PLAYER_BAT_ID, nameandIdModel.getId());
                        contentValues.put(LocalDbConstant.CURRE_MATCH_PLAYER_MAT_ID, matchId);
                        long id = sqLiteDatabase.insert(LocalDbConstant.CURR_MATCH_PLAYER_TABLE, null, contentValues);
                        if (id != -1) {

                            Log.d(TAG, "inserted");
                        }
                        ++count;
                    }
                    Intent intent = new Intent(StartMatchEnterPlayerDetail.this, MainActivity.class);
                    intent.putExtra(CONSTANT.CURRENT_FRAGMENT, 1);
                    intent.putExtra("MatchId", matchId);
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "Please Select Different Players", Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e(TAG, requestCode + ",requestcode");
        if (requestCode == 100) {
            if (resultCode == Activity.RESULT_OK) {
                NameandIdModel nameandIdModel = new NameandIdModel();
                nameandIdModel.setId(data.getStringExtra("id"));
                Log.d("response", data.getStringExtra("id") + "...1");
                nameandIdModel.setName(data.getStringExtra("name"));
                playerFirst.setText(data.getStringExtra("name"));
                nameandIdModelList.add(nameandIdModel);
            }

        }
        if (requestCode == 101) {
            if (resultCode == Activity.RESULT_OK) {
                NameandIdModel nameandIdModel = new NameandIdModel();
                nameandIdModel.setId(data.getStringExtra("id"));
                Log.d("response", data.getStringExtra("id") + "...2");

                nameandIdModel.setName(data.getStringExtra("name"));
                playerSecond.setText(data.getStringExtra("name"));
                nameandIdModelList.add(nameandIdModel);
            }
        }
    }
}
