package com.cricklub.WebReq;

import android.util.Log;

import com.cricklub.CONSTANT;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class WebReqClass {
    public static WebReqClass webclient;
    public static int DEFAULT_TIMEOUT = 300000 * 1000;

    public static AsyncHttpClient client;

    static {
        client = new AsyncHttpClient();
        client.setConnectTimeout(DEFAULT_TIMEOUT);


    }

    public static WebReqClass getInstance() {
        if (webclient != null) {
            webclient = new WebReqClass();
        }
        return webclient;
    }

    public static void get(String url, RequestParams requestParams, AsyncHttpResponseHandler responseHandler) {
        Log.e("response",CONSTANT.BASE_URL+url);
        client.get(CONSTANT.BASE_URL+url,requestParams,responseHandler);
    }
    public static void post(String url,RequestParams requestParams,AsyncHttpResponseHandler responseHandler)
    {        Log.e("response",CONSTANT.BASE_URL+url);

        client.post(CONSTANT.BASE_URL+url,requestParams,responseHandler);
    }


}
