package com.cricklub.models;

public class SelectedPlayer {

    private String id;
    private String playerName;
    private boolean isSelected;
    private String phone_no, is_app_user;



    public SelectedPlayer(String id, String playerName, boolean isSelected, String phone_no, String is_app_user) {
        this.id = id;
        this.playerName = playerName;
        this.isSelected = isSelected;
        this.phone_no = phone_no;
        this.is_app_user = is_app_user;
    }

    public SelectedPlayer(String id, String playerName, boolean isSelected) {
        this.id = id;
        this.playerName = playerName;
        this.isSelected = isSelected;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getPhone_no() {
        return phone_no;
    }

    public void setPhone_no(String phone_no) {
        this.phone_no = phone_no;
    }

    public String getIs_app_user() {
        return is_app_user;
    }

    public void setIs_app_user(String is_app_user) {
        this.is_app_user = is_app_user;
    }
}
