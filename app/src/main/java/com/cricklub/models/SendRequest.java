package com.cricklub.models;

public class SendRequest {

    private String teamID, teamUnique, name, city, logo, profileImage, created_at;

    public SendRequest(String teamID, String teamUnique, String name, String city, String logo, String profileImage, String created_at) {
        this.teamID = teamID;
        this.teamUnique = teamUnique;
        this.name = name;
        this.city = city;
        this.logo = logo;
        this.profileImage = profileImage;
        this.created_at = created_at;
    }

    public String getTeamID() {
        return teamID;
    }

    public void setTeamID(String teamID) {
        this.teamID = teamID;
    }

    public String getTeamUnique() {
        return teamUnique;
    }

    public void setTeamUnique(String teamUnique) {
        this.teamUnique = teamUnique;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}